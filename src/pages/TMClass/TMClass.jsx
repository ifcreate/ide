import React from 'react';
import InputCamera from 'components/InputCamera';
import ClassTrain from 'components/ClassTrain';
import Output from 'components/Indicator';
import FullScreen from 'components/FullScreen';
import ml5 from '../../lib/ml5.latest.min';
import ws from '../../lib/websocket';
import 'style/tm-class.less';

const CLASS_NUM = 3;

export default class TMClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            video: null,
            classInfo: [
                {
                    name: 'Class1',
                    color: '#00C3B3',
                },{
                    name: 'Class2',
                    color: '#FF6F79',
                },{
                    name: 'Class3',
                    color: '#7CD1FF',
                },
            ],
            classIndex: null,
            playing: false,
            fullScreen: false,
            botActions: [
                {name: 'Forward', op: 'go', inline: false},
                {name: 'Left', op: 'left', inline: true},
                {name: 'Right', op: 'right', inline: true},
                {name: 'Back', op: 'back', inline: false},
            ],
            wsStatus: '...',
        };
        this.knn = new ml5.KNNClassifier();
        this.featureExtractor = ml5.featureExtractor('MobileNet', ()=>{
            console.log('featureExtractor mobilenet loaded');
        });
        window.knn = this.knn;
        window.featureExtractor = this.featureExtractor;
        this.wsStatusCb = this.wsStatusCb.bind(this);
        ws.createWebsocket(this.wsStatusCb);
    }
    componentDidMount() {
        //
    }
    registerVideo(video) {
        this.setState({video});
    }
    predict () {
        if (this.totalCount() <= 0) {
            console.error('There is no examples in any label');
            return;
        }
        const features = this.featureExtractor.infer(this.state.video);
        this.knn.classify(features, (err, res)=>this.gotResults(err, res));
    };
    gotResults(err, results) {
        if (err) {
            console.error(err);
        }
        console.log(results);
        const {classIndex} = results;
        if (classIndex < 0) return;
        this.setState({
            classIndex,
        });
        if(window.ws!=null) {
            ws.websocketSend('' + (classIndex + 1));
        }
        if (this.state.playing) {
            setTimeout(() => this.predict(), 700);
        }
        return classIndex;
    }
    encode() {

    }
    totalCount() {
        const map = this.knn.getCountByLabel();
        if (!this.knn || !map) return 0;
        let res = 0;
        for (var i in map) {
            res += map[i];
        }
        return res;
    }
    play() {
        this.setState({playing: true,});
        this.predict();
    }
    stop() {
        this.setState({playing: false,})
    }
    demo() {
        this.setState({fullScreen: true,})
    }
    exitDemo() {
        this.setState({fullScreen: false,})
    }
    move(op) {
        ws.websocketSend(op, this.wsStatusCb);
    }
    wsStatusCb(sta) {
        this.setState({wsStatus: sta});
    }
    onTouchStart(op){
        this.move(op);
        this.timeOutEvent = setTimeout(() => {
            this.opInterval = setInterval(()=>{
                this.move(op);
            }, 500);
        }, 400);

    }
    onTouchEnd() {
        clearTimeout(this.timeOutEvent);
        clearInterval(this.opInterval);
        return false;
    }
    render() {
        return (
            <div className="app-wrap">
                <div className="menu">
                    <div className="app-title">Teachable Machine</div>
                    <div className="button-wrap">
                        <button onClick={() => {this.play()}}>Play</button>
                    </div>
                    <div className="button-wrap">
                        <button onClick={() => {this.stop()}}>Stop</button>
                    </div>
                    <div className="button-wrap">
                        <button onClick={()=>this.demo()}>Demo</button>
                    </div>
                    <div className="bot-move">
                        <p>{this.state.wsStatus}</p>
                        {this.state.botActions.map((item) => {
                            return <div className={`button-wrap${item.inline ? ' button-row':''}`}>
                                <button
                                    // onClick={()=>{this.move(item.op)}}
                                    onTouchStart={()=>{this.onTouchStart(item.op)}}
                                    onTouchEnd={()=>{this.onTouchEnd(item.op)}}
                                    onMouseDown={()=>{this.onTouchStart(item.op)}}
                                    onMouseUp={()=>{this.onTouchEnd(item.op)}}
                                >{item.name}</button>
                            </div>
                        })}
                    </div>
                </div>
                <div className="main-content-wrap">
                    <div className="app-top">
                        <InputCamera
                            registerVideo={(v)=>this.registerVideo(v)}
                            isApp
                        />
                    </div>
                    <div className="app-bottom-wrap">
                        <div className="tab-label">Teachable Machine</div>
                        <div className="app-bottom">
                            {(new Array(CLASS_NUM).fill(0)).map((item, index) => {
                                return <ClassTrain
                                    index={index}
                                    knn={this.knn}
                                    featureExtractor={this.featureExtractor}
                                    video={this.state.video}
                                    classInfo={this.state.classInfo[index]}
                                /> //todo add key
                            })}
                            <Output
                                classInfo={this.state.classInfo[this.state.classIndex]}
                            />
                        </div>
                    </div>
                </div>
                {this.state.fullScreen && <FullScreen close={()=>this.exitDemo()} />}
            </div>
        )
    }
}
