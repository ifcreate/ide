import React from 'react';
import ReactDOM from 'react-dom';
import App from './TMReg.jsx';
import { AppContainer } from 'react-hot-loader'

const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer>,
        document.getElementById('app')
    )
}

render(App)

if (module.hot) {
    module.hot.accept('./TMReg.jsx', () => { render(App) })
} else {
    console.log('module not hot')
}
