import React from 'react';
import InputCamera from 'components/InputCamera';
import RegressionTrain from 'components/RegressionTrain';
import Output from 'components/Indicator';
import ml5 from '../../lib/ml5.latest.min';
import ws from '../../lib/websocket';
import 'style/tm-reg.less'

export default class TMReg extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            video: null,
            res: '--',
            currPos: 0,
            regressor: null,
        };
        this.featureExtractor = ml5.featureExtractor('MobileNet', ()=>console.log('mobileNet ready'));
        // ml5.
        ws.createWebsocket();
    }
    componentDidMount() {
        console.log('TMReg mounted');
    }
    registerVideo(video) {
        console.log('register video');
        // this.regressor = this.featureExtractor.regression(video, ()=>console.log('videoReady'));
        // window.regressor = this.regressor;
        this.setState({
            video,
            regressor: this.featureExtractor.regression(video, ()=>console.log('videoReady')),
        });
    }
    gotResults(err, res) {
        if (err) {
            console.error(err);
        }
        let scaled = Math.floor(res * 100);
        scaled = scaled > 100 ? 100
            : scaled < 0 ? 0
                : scaled;
        ws.websocketSend(scaled);
        this.setState({
            res: scaled+'%',
        });
    }
    resetRes() {
        this.setState({
            res: '--',
        })
    }
    render() {
        return (
            <div className="app-wrap">
                <div className="menu">
                    <div className="app-title">Teachable Machine</div>
                    <div className="button-wrap">
                        <button>Play</button>
                    </div>
                    <div className="button-wrap">
                        <button>Demo</button>
                    </div>
                </div>
                <div className="main-content-wrap">
                    <div className="app-top">
                        <InputCamera
                            registerVideo={(v)=>this.registerVideo(v)}
                            isApp
                        />
                    </div>
                    <div className="app-bottom-wrap">
                        <div className="tab-label">Teachable Machine</div>
                        <div className="app-bottom">
                            <RegressionTrain
                                knn={this.state.regressor}
                                video={this.state.video}
                                gotResults={(err, res)=>{this.gotResults(err, res)}}
                                resetRes={()=>{this.resetRes()}}
                            />
                            <Output
                                classInfo={{
                                    name: this.state.res,
                                    color: '#FF6F79',
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
