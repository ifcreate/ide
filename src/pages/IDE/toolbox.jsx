import React from 'react';

//IC_Result id: "Nqx/r$L-WE;7F_o)Sh/K"
//SC_Result id: "96l1.g5`G:-[Y^p1e[9_"
//face : id="[CYNXEg-NJyO@^ajD=q,"

export default function () {
    return (
        <xml id="toolbox" style={ { display: 'none' } }>
            <category name="AI Algorithm" colour="0">
                <block type="image_classifier"></block>
                <block type="image_class"></block>
                <block type="vector_face_rec"></block>
                <block type="sentence_classifier"></block>
                <block type="sentence_class"></block>
                <block type="regression"></block>
                <block type="reinforcement"></block>
                <block type="code_block"></block>
                <block type="start_code_block"></block>
                <block type="stop_code_block"></block>
            </category>
            <category name="Control" colour="100">
                <block type="controls_if"></block>
                <block type="logic_compare"></block>
                {/* <block type="start_image_classifier"></block>
                <block type="stop_image_classifier"></block>
                <block type="start_sentence_classifier"></block>
                <block type="stop_sentence_classifier"></block>*/}
            </category>
            <category name="Media" colour="150">
                <block type="camera"></block>
                <block type="speech_to_text"></block></category>
            <category name="UI" colour="200">
                <block type="curling"></block>
                <block type="set_image"></block></category>
            <category name="Text" colour="250">
                <block type="text"></block></category>
            <category name="Robot" colour="300">
                <block type="dinosaur_stand"></block>
                <block type="dinosaur_jump"></block>
                <block type="dinosaur_duck"></block>
                <block type="robi_turn_left"></block>
                <block type="robi_turn_right"></block>
                <block type="robi_move_forward"></block>
                <block type="robi_move_back"></block>
                <block type="robi_say"></block>
                <block type="vector_go_at"></block>
                <block type="vector_go"></block>
                <block type="vector_turn"></block>
                <block type="lift_arm_to"></block>
                <block type="raise_head_to"></block>
            </category>
            <category name="Operators" colour="350"></category>
            <category name="Variables" colour="40" custom="VARIABLE"></category>
            <category name="AI Apps" colour="50" expanded="false">
                <category name="Robi Raise Hands" colour="50">
                    <block type="start_code_block">
                        <field name="NAME">raise hands</field>
                        <next>
                            <block type="code_block">
                                <field name="code_block_name">raise hands</field>
                                <field name="can_repeat">TRUE</field>
                                <field name="every_ms">200</field>
                                <statement name="sta_input">
                                    <block type="camera">
                                        <next>
                                            <block type="image_classifier">
                                                <statement name="classes">
                                                    <block type="image_class">
                                                        <field name="image_class_name">Left</field>
                                                        <next>
                                                            <block type="image_class">
                                                                <field name="image_class_name">Right</field>
                                                                <next>
                                                                    <block type="image_class">
                                                                        <field name="image_class_name">Both</field>
                                                                        <next>
                                                                            <block type="image_class">
                                                                                <field name="image_class_name">None</field>
                                                                            </block>
                                                                        </next>
                                                                    </block>
                                                                </next>
                                                            </block>
                                                        </next>
                                                    </block>
                                                </statement>
                                                <statement name="res handler">
                                                    <block type="controls_if">
                                                        <mutation elseif="3"></mutation>
                                                        <value name="IF0">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Left</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO0">
                                                            <block type="set_image">
                                                                <field name="block type">opimage</field>
                                                                <field name="file name">leftHand.png</field>
                                                            </block>
                                                        </statement>
                                                        <value name="IF1">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Right</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO1">
                                                            <block type="set_image">
                                                                <field name="block type">opimage</field>
                                                                <field name="file name">rightHand.png</field>
                                                            </block>
                                                        </statement>
                                                        <value name="IF2">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Both</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO2">
                                                            <block type="set_image">
                                                                <field name="block type">opimage</field>
                                                                <field name="file name">bothHands.png</field>
                                                            </block>
                                                        </statement>
                                                        <value name="IF3">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">None</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                    </block>
                                                </statement>
                                            </block>
                                        </next>
                                    </block>
                                </statement>
                            </block>
                        </next>
                    </block>
                </category>
                <category name="Dinosaur Game" colour="50">
                    <block type="start_code_block">
                        <field name="NAME">dinosaur</field>
                        <next>
                            <block type="code_block">
                                <field name="code_block_name">dinosaur</field>
                                <field name="can_repeat">TRUE</field>
                                <field name="every_ms">200</field>
                                <statement name="sta_input">
                                    <block type="camera">
                                        <next>
                                            <block type="image_classifier">
                                                <statement name="classes">
                                                    <block type="image_class">
                                                        <field name="image_class_name">Jump</field>
                                                        <next>
                                                            <block type="image_class">
                                                                <field name="image_class_name">Duck</field>
                                                                <next>
                                                                    <block type="image_class">
                                                                        <field name="image_class_name">Stand</field>
                                                                    </block>
                                                                </next>
                                                            </block>
                                                        </next>
                                                    </block>
                                                </statement>
                                                <statement name="res handler">
                                                    <block type="controls_if">
                                                        <mutation elseif="2"></mutation>
                                                        <value name="IF0">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Jump</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO0">
                                                            <block type="dinosaur_jump"></block>
                                                        </statement>
                                                        <value name="IF1">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Duck</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO1">
                                                            <block type="dinosaur_duck"></block>
                                                        </statement>
                                                        <value name="IF2">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Stand</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO2">
                                                            <block type="dinosaur_stand"></block>
                                                        </statement>
                                                    </block>
                                                </statement>
                                            </block>
                                        </next>
                                    </block>
                                </statement>
                            </block>
                        </next>
                    </block>
                </category>
                <category name="Robi Walking" colour="50">
                    <block type="start_code_block">
                        <field name="NAME">walking</field>
                        <next>
                            <block type="code_block">
                                <field name="code_block_name">walking</field>
                                <field name="can_repeat">TRUE</field>
                                <field name="every_ms">200</field>
                                <statement name="sta_input">
                                    <block type="camera">
                                        <next>
                                            <block type="image_classifier">
                                                <statement name="classes">
                                                    <block type="image_class">
                                                        <field name="image_class_name">Up</field>
                                                        <next>
                                                            <block type="image_class">
                                                                <field name="image_class_name">Down</field>
                                                                <next>
                                                                    <block type="image_class">
                                                                        <field name="image_class_name">Left</field>
                                                                        <next>
                                                                            <block type="image_class">
                                                                                <field name="image_class_name">Right</field>
                                                                            </block>
                                                                        </next>
                                                                    </block>
                                                                </next>
                                                            </block>
                                                        </next>
                                                    </block>
                                                </statement>
                                                <statement name="res handler">
                                                    <block type="controls_if">
                                                        <mutation elseif="3"></mutation>
                                                        <value name="IF0">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Up</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO0">
                                                            <block type="robi_move_forward"></block>
                                                        </statement>
                                                        <value name="IF1">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Down</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO1">
                                                            <block type="robi_move_back"></block>
                                                        </statement>
                                                        <value name="IF2">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Left</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO2">
                                                            <block type="robi_turn_left"></block>
                                                        </statement>
                                                        <value name="IF3">
                                                            <block type="logic_compare">
                                                                <field name="OP">EQ</field>
                                                                <value name="A">
                                                                    <block type="variables_get">
                                                                        <field name="VAR" id="Nqx/r$L-WE;7F_o)Sh/K" variabletype="">IC_Result</field>
                                                                    </block>
                                                                </value>
                                                                <value name="B">
                                                                    <block type="text">
                                                                        <field name="TEXT">Right</field>
                                                                    </block>
                                                                </value>
                                                            </block>
                                                        </value>
                                                        <statement name="DO3">
                                                            <block type="robi_turn_right"></block>
                                                        </statement>
                                                    </block>
                                                </statement>
                                            </block>
                                        </next>
                                    </block>
                                </statement>
                            </block>
                        </next>
                    </block>
                </category>
                <category name="Robi Race" colour="50">
                    <block type="camera">
                        <next>
                            <block type="regression"></block>
                        </next>
                    </block>
                </category>
                <category name="Robi Reception" colour="50">
                    <block type="start_code_block" id="$JmWi9dP^y#c+a0bvS:0" x="16" y="10"><field name="NAME">face_rec</field><next><block type="code_block" id="TI[u[s~^g@{(-cu]8/2l"><field name="code_block_name">face_rec</field><field name="can_repeat">FALSE</field><field name="every_ms">200</field><statement name="sta_input"><block type="vector_face_rec" id="@FczSuXb0)08Vc(aywX."><statement name="res handler"><block type="controls_if" id="ag9X{Rvy1P:1ou;WR+mz"><mutation elseif="2" else="1"></mutation><value name="IF0"><block type="logic_compare" id="q5%f[r|YX`4U_xL:oye."><field name="OP">EQ</field><value name="A"><block type="variables_get" id="|CE1@KrvL2I.fyQ?9ooa"><field name="VAR" id="[CYNXEg-NJyO@^ajD=q," variabletype="">face</field></block></value><value name="B"><block type="text" id="m{[Qm1I[`Pqfg1%;Lv4F"><field name="TEXT">jack</field></block></value></block></value><statement name="DO0"><block type="set_image" id="`BECV9:ZJc-LKd,fOBXp"><field name="block type">opimage</field><field name="file name">leftHand.png</field></block></statement><value name="IF1"><block type="logic_compare" id="pLu#Y=D|OfggDRwzR?;z"><field name="OP">EQ</field><value name="A"><block type="variables_get" id="%^cmo#l|,wb^,wVK%}`N"><field name="VAR" id="[CYNXEg-NJyO@^ajD=q," variabletype="">face</field></block></value><value name="B"><block type="text" id="ha7JK?v(y?UG}jurtfD3"><field name="TEXT">rose</field></block></value></block></value><statement name="DO1"><block type="set_image" id="`Am.{Y-n@wO`U;moX#pL"><field name="block type">opimage</field><field name="file name">rightHand.png</field></block></statement><value name="IF2"><block type="logic_compare" id="j/eX:ndz6~,b9jS_xr8."><field name="OP">EQ</field><value name="A"><block type="variables_get" id="ujVFd3,^);tq@Ug6B8+5"><field name="VAR" id="[CYNXEg-NJyO@^ajD=q," variabletype="">face</field></block></value><value name="B"><block type="text" id="G2.M#O{Y,x|2/BA#}w[#"><field name="TEXT">chan</field></block></value></block></value><statement name="DO2"><block type="set_image" id="M,vNaM_:Pr=e5zX^VO=b"><field name="block type">opimage</field><field name="file name">bothHands.png</field><next><block type="stop_code_block" id="{aNW{1[ARg9i*R^x+|8B"><field name="NAME">face_rec</field><next><block type="start_code_block" id="%f0qRUz(iK%A@}Om)O8$"><field name="NAME">listen</field></block></next></block></next></block></statement><statement name="ELSE"><block type="robi_say" id=")~dDP8[to7])#%k}#ebV"><value name="robi_say"><block type="text" id="OXw62f@)V:gNMwT6Fvjj"><field name="TEXT">Sorry, we didn't find your reservation on record</field></block></value></block></statement></block></statement></block></statement><next><block type="code_block" id="$~[*Y/8|Ajj`XI8JBTQw"><field name="code_block_name">listen</field><field name="can_repeat">FALSE</field><field name="every_ms">200</field><statement name="sta_input"><block type="speech_to_text" id="pe9r^M*zI7X=CHQ}2*~U"><statement name="res handler"><block type="sentence_classifier" id="w_s@/lc7(8`Cx:@@9/C~"><statement name="classes"><block type="sentence_class" id="yGs[:IQ]MGuDwm=A!t^x"><field name="sentence_class_name">window</field><next><block type="sentence_class" id="9/Ca]f33PO39!:/Y$tGD"><field name="sentence_class_name">six</field></block></next></block></statement><statement name="res handler"><block type="controls_if" id="eb)]?_QWr:p(n(avO33z"><mutation elseif="1"></mutation><value name="IF0"><block type="logic_compare" id="Drt`;sNvkc,(MiwvP0{/"><field name="OP">EQ</field><value name="A"><block type="variables_get" id="{Q!()KbK}59W_onsY%)+"><field name="VAR" id="96l1.g5`G:-[Y^p1e[9_" variabletype="">SC_Result</field></block></value><value name="B"><block type="text" id="QL]z}eEK6`9_}fn+a2KB"><field name="TEXT">window</field></block></value></block></value><statement name="DO0"><block type="robi_say" id="SGdB+C$Kx:}MWR*-zvoa"><value name="robi_say"><block type="text" id="Yj4dKv%$X_}.zz_s7SBZ"><field name="TEXT">Perfect, we got one right now, follow me please</field></block></value><next><block type="lift_arm_to" id="d1OEFp~tq(C48d;qqL_2"><field name="speed">42</field><next><block type="vector_turn" id="lkP+2:7CF0?.Syi)i.c!"><field name="speed">180</field><next><block type="vector_go" id="fV(.84~m`91nqomSg3]V"><field name="speed">100</field><next><block type="vector_turn" id="MC+ng.%[:4Q%Vhw!j86C"><field name="speed">70</field><next><block type="vector_go" id="o:^3?o1V)Rb#qBYfdGb@"><field name="speed">290</field><next><block type="robi_say" id="z)#Yp#+@_CJsf0P6Kj;o"><value name="robi_say"><block type="text" id="CZXcs#EO0%*oIj~zFTL="><field name="TEXT">This way please</field></block></value><next><block type="vector_turn" id="i]xJTO]]NFv4!Fs;MLgq"><field name="speed">-70</field><next><block type="vector_go" id="]~@e*qi//=V6a(0[DIU5"><field name="speed">150</field><next><block type="vector_turn" id="p_Eb2GPusbLeaeaShZ^U"><field name="speed">40</field><next><block type="lift_arm_to" id="0#z[71+aUh7]Jby+D#_)"><field name="speed">0</field><next><block type="robi_say" id="Yb+mtyHbz`!AD}_7jttP"><value name="robi_say"><block type="text" id="fAU~1K^%%EvMsAXshX,W"><field name="TEXT">Here we go, Right beside the window, enjoy</field></block></value></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></statement><value name="IF1"><block type="logic_compare" id="O1{Fw(zoCDd*INGV2qI6"><field name="OP">EQ</field><value name="A"><block type="variables_get" id="Hk*jEE_dg/_o-T@6/@?#"><field name="VAR" id="96l1.g5`G:-[Y^p1e[9_" variabletype="">SC_Result</field></block></value><value name="B"><block type="text" id="e)-PF#cyiFohOX:[Ry^F"><field name="TEXT">table for six</field></block></value></block></value><statement name="DO1"><block type="lift_arm_to" id="gWLn6^GZy|X$_;CfnNvq"><field name="speed">90</field><next><block type="robi_say" id="Fjf|pvHccDQQX:$27=?H"><value name="robi_say"><block type="text" id="-PAb];~cCYg56p[Zu3Yo"><field name="TEXT">sorry, can you guys wait a moment</field></block></value><next><block type="lift_arm_to" id="F;!7wXPmY[3JXm;0hM,t"><field name="speed">0</field></block></next></block></next></block></statement></block></statement></block></statement></block></statement></block></next></block></next></block>
                </category>
                <category name="Curling Game" colour="50">
                    <block type="curling">
                        <next>
                            <block type="reinforcement"></block>
                        </next>
                    </block>
                </category>
            </category>
        </xml>
    )
}
