import React from 'react';
import InputCamera from 'components/InputCamera';
import ClassTrain from 'components/ClassTrain';
import SentenceClass from 'components/SentenceClass';
import RegressionTrain from 'components/RegressionTrain';
import Indicator from 'components/Indicator';
import OutputImage from 'components/OutputImage';
import Curling from 'components/Curling';
import SpeechToText from 'components/SpeechToText';
import FullScreenCheck from 'components/FullScreenCheck';
import UICompToolBar from 'components/UICompToolBar';
import GenedCode from 'components/GenedCode';
import FullScreen from 'components/FullScreen';
import ReinforcementLearning from 'components/ReinforcementLearning';
import DinosaurGame from 'components/DinosaurGame';
import RobiWalking from 'components/RobiWalking';
import RobiRace from 'components/RobiRace';
import Toolbox from './toolbox'
import ml5 from 'lib/ml5.latest.robizlab.min';
import ws from '../../lib/websocket';
import {parseQuery} from '../../lib/utils';
import { Loading, Message, Button, Checkbox } from 'element-react';
import 'element-theme-default';
import sentenceClassifier from '../../lib/SentenceClassifier';
import axios from 'axios';
import {getSpeedTime} from 'lib/CurlingRobot'
import mqtt from  'mqtt';
import mqttParams from "lib/mqtt";
import "lib/AudioRecorder";
import 'style/ide.less'


const COLORS = ['#00C3B3', '#FF6F79', '#7CD1FF', '#8e99c9'];
const IMAGES = ['rock.png', 'scissors.png', 'paper.png'];
// classifiers will create tab, classes only show when corresponding tab is present and active.
const TAB_NAME = {
    image_classifier: 'Image Classifier',
    sentence_classifier: 'Sentence Classifier',
    regression: 'Regression',
    reinforcement: 'Reinforcement',
};

const WHICH_TAB = {
    image_class: 'image_classifier',
    indicator_imageclass: 'image_classifier',

    sentence_class: 'sentence_classifier',
    indicator_sentenceclass: 'sentence_classifier',
    button_sentence_train: 'sentence_classifier',

    regression: 'regression',
    indicator_regression: 'regression',

    reinforcement: 'reinforcement',
}

//blockly types which will change react UI.
const CUSTOM_TYPES = [
    'camera',
    'image_classifier', 'image_class',
    'sentence_classifier', 'sentence_class',
    'curling', 'speech_to_text', 'set_image',
    'regression', 'reinforcement', 'dinosaur_jump',
    'indicator_sentenceclass', 'indicator_regression',
    'robi_turn_left', 'robi_turn_right', 'robi_move_forward', 'robi_move_back',
];
const hasInputField = ['image_class', 'sentence_class']; // type that need to sync input field
const NeedSyncInput = (type) => hasInputField.indexOf(type) > -1;

export default class IDE extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            video: null, //video elmt
            classInfo: [ // image classes
                // {name: '', color: '', image: 'rock.png', id: ''}
            ],
            sentenceClassInfo:[
                // {name: '', color: '', desc: 'description', id: '', caseInput: 'text', cases: ['case1', 'case2']}
            ],
            classIndex: null, // image classifier index.
            playing: false,
            fullScreen: false,
            //blocks
            uiBlocks: ['image_class', 'sentence_class'],

            curlingTarget: 1, // curling game target position 1-5
            currColor: 0,
            params: parseQuery(location.search),
            fs_cam: false, // fullscreen mode, cam
            fs_image: false,
            res: '--', // regression result string, 30%
            regVal: 50, // regression result val 0-100
            modelLoadingHistory: new Set(), // models loaded before
            modelLoading: new Set(),
            speechRes: '',
            sendToRobot: false,
            robotId: '',
            currTab: '',
            curlingRes: {},
            triggerProgress: '',
        };
        window.wsApi = ws;
        // ws.createWebsocket();
        this.removeId = this.removeId.bind(this);
        this.regressionGotResults = this.regressionGotResults.bind(this);
        window.setImageClassIndex = this.setImageClassIndex.bind(this);
        window.ideSetter = this.ideSetter.bind(this);
        window.ideGetter = this.ideGetter.bind(this);
        // register robot actions
        this.regMax = 0;
        this.regMin = 100;
    }
    componentDidMount() {
        window.ml5 = ml5;
        window.wsListener = this.wsOnChange.bind(this); // workspace on change
        this.injectWorkspace();
        const game = document.getElementById("game"+this.state.params.game);
        game && Blockly.Xml.domToWorkspace(game, window.workspace);

        //fullscreen mode
        // this.fullScreen()
    }
    fullScreen() {
        var docElm = document.documentElement;
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    }
    closeFullScreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
    // -----------------IDE only---------------------
    injectWorkspace() {
        const options = {
            toolbox: document.getElementById('toolbox'),
            collapse : true,
            comments : true,
            disable : true,
            maxBlocks : Infinity,
            trashcan : true,
            horizontalLayout : false,
            toolboxPosition : 'start',
            css : true,
            media: '../src/lib/blockly/media/',
            rtl : false,
            scrollbars : true,
            sounds : true,
            oneBasedIndex : true,
            zoom : {
                controls : true,
                wheel : true,
                startScale : 0.9,
                maxScale : 3,
                minScale : 0.3,
                scaleSpeed : 1.2
            }
        };
        window.workspace = Blockly.inject('blocklyDiv', options);
        // const workspaceBlocks = document.getElementById("workspaceBlocks");
        /* Load blocks to workspace. */
        // Blockly.Xml.domToWorkspace(workspaceBlocks, workspace);

        workspace.addChangeListener(window.wsListener);
        //code gen
        function myUpdateFunction(event) {
            const code = Blockly.JavaScript.workspaceToCode(workspace);
            window.code = code;
            const el = document.querySelector('.codearea');
            (el||{}).value = code;
            // Function(code)(); // run code
        }
        workspace.addChangeListener(myUpdateFunction);
    }
    save() {
        const xml = Blockly.Xml.workspaceToDom(window.workspace);
        const xml_text = Blockly.Xml.domToText(xml);
        localStorage.setItem('workspace', xml_text);
        // console.log(xml_text)
    }
    load() {
        const xml_text = localStorage.getItem('workspace');
        const xml = Blockly.Xml.textToDom(xml_text);
        Blockly.Xml.domToWorkspace(xml, window.workspace);
    }
    getCustomTypes(e) {
        try {
            const xml = e.xml || e.oldXml;
            const res = [];
            const html = (xml.outerHTML || '');
            let arr = html.split('><');
            arr.forEach((item, index) => { // attach field value to needsync types
                if (item.indexOf(' type') > -1) {
                    const a = item.split(' ');
                    const name = a[1].split('"')[1];
                    if (NeedSyncInput(name)) {
                        arr[index] = item + ` field="${arr[index + 1].split(/>|</)[1]}"`; //
                    }
                }
            });
            arr = arr.filter(item => item.indexOf(' type') > -1); // get all types
            arr.forEach((item) => { // get all custom types
                const a = item.split(' ');
                const name = a[1].split('"')[1];
                const id = a[2].split('"')[1];
                let field = '';
                if (CUSTOM_TYPES.indexOf(name) > -1) {
                    if (NeedSyncInput(name)) {
                        field = a[a.length - 1] && a[a.length - 1].split('"')[1]; //field is appened last
                    }
                    res.push({name, id, field})
                }
            });
            return res;
        } catch (e) {
            console.log(e.message);
        }
    }

    handleCreate(name) { // supp single string or string array
        const arr = [...this.state.uiBlocks];
        let currTab = null;
        if (typeof name === 'string') {
            if (arr.indexOf(name) < 0) {
                arr.push(name);
            }
            if (TAB_NAME[name]) {
                currTab = name;
            }
        } else if (name instanceof Array) {
            name.forEach((item) => {
                if (arr.indexOf(item) < 0) {
                    arr.push(item);
                }
                if (TAB_NAME[item]) {
                    currTab = item;
                }
            });
        }
        this.setState({
            uiBlocks: arr,
            currTab: currTab || this.state.currTab,
        })
    }

    handleDelete(name) {
        // update uiBlocks, loading states,
        let arr = [...this.state.uiBlocks];
        const set = new Set(this.state.modelLoading);
        let newCurrTab = this.state.currTab;
        if (typeof name === 'string') {
            arr = arr.filter(i => i !== name);
            set.delete(name);
            if (TAB_NAME[name]) {
                if (name === this.state.currTab) {
                    const tabsAfterDel = this.currTabs().filter(i => i !== name);
                    newCurrTab = tabsAfterDel[0] || ''; // to first tab or empty
                }
            }
        } else if (name instanceof Array) {
            name.forEach((namei) => {
                arr = arr.filter(i => i !== namei);
                set.delete(namei);
                if (TAB_NAME[namei]) {
                    if (namei === this.state.currTab) {
                        const tabsAfterDel = this.currTabs().filter(i => i !== namei);
                        newCurrTab = tabsAfterDel[0] || ''; // to first tab or empty
                    }
                }
            });
        }
        this.setState({
            uiBlocks: arr,
            modelLoading: set,
            currTab: newCurrTab,
        })
    }

    currTabs() {
        return this.state.uiBlocks.filter(i=>TAB_NAME[i]);
    }

    setId(name, id) {
        Object.assign({}, this.state[name], {[id]: 1})
    }

    removeId(name, id) {
        const copy = Object.assign({}, this.state[name]);
        delete copy[id];
        return copy;
    }

    regCloseCam(f) {
        this.closeCam = f;
    }

    toolOnClick(name) { //toggle ui block
        let arr = [...this.state.uiBlocks];
        if (arr.indexOf(name) > -1) {
            arr = arr.filter(i => i !== name);
        } else {
            arr.push(name);
        }
        this.setState({
            uiBlocks: arr,
        })
    }

    toggle(item) {
        this.setState({
            [`${item}`]: !this.state[`${item}`],
        })
    }

    registerVideo(video) {
        this.setState({video});
    }

    play() {
        this.setState({playing: true,});
        window.playing = true;
        window.currImgRes = null; // reset image classifier result cache.
        // this.predict();
        Function(code)();
    }

    stop() {
        this.setState({
            playing: false,
            speechRes: '',
        });
        window.playing = false;
        window.codeBlockIds && Object.keys(window.codeBlockIds).forEach((key) => {
            clearInterval(window.codeBlockIds[key]);
        });
        window.sttHideCount && window.sttHideCount();
        window.socket && window.socket.disconnect();
        this.setState({
            sendToRobot: false,
        })

        window.client && window.client.end();
        window.client = null;
    }

    demo() {
        this.setState(() => {
            return {fullScreen: true};
        }, ()=>{
            // document.querySelector('#fs-cam').srcObject = document.querySelector('#inputcam').srcObject;
        })
        this.fullScreen();
    }

    exitDemo() {
        this.setState({fullScreen: false,})
        this.closeFullScreen();
    }

    regressionPredict() {
        this.regressor.predict(this.regressionGotResults);
    }

    regressionGotResults(err, res) {
        if (err) {
            console.error(err);
            this.stopReg();
            Message.error('Add examples, Train then Predict');
            return;
        }
        let scaled = Math.floor(res * 100);
        if (scaled < this.regMin) { //not sure why res will be >1. so we using dynamic range
            this.regMin = scaled;
        }
        if (scaled > this.regMax) {
            this.regMax = scaled;
        }
        const nume = scaled - this.regMin;
        const deno = Math.max(10, (this.regMax - this.regMin));
        scaled = Math.floor((nume/deno) * 100);
        if (window.sendToRobot) {
            // ws.websocketSend(scaled); // our own robot
            if (window.client && window.client.connected) {
                window.client.publish(`/admin/${this.state.robotId}`, `${scaled}`, (error) => {
                    console.log(error || 'Publish Success')
                })
            } else {
                console.log('mqtt client not connected')
            }
            axios.get(`http://127.0.0.1:8080/forward_turn?deg=${scaled - 50}`); // to vector
        }
        this.setState({
            res: scaled+'%',
            regVal: scaled,
        });
        if (this.state.regPredicting) {
            const delay = window.sendToRobot ? 500 : 0;
            setTimeout(() => this.regressionPredict(), delay);
        }
    }

    startReg() {
        this.setState({regPredicting: true});
    }

    stopReg() {
        this.setState({regPredicting: false});
    }

    regressionResetRes() {
        this.setState({
            res: '--',
        })
    }

    alreadyLoaded(name) {
        return this.state.modelLoadingHistory.has(name);
    }

    loadingCb(tar) {
        console.log('model loaded');
        const s = new Set(this.state.modelLoading);
        s.delete(tar);
        const h = new Set(this.state.modelLoadingHistory);
        h.add(tar);
        this.setState({
            modelLoading: s,
            modelLoadingHistory: h,
        })
    }

    curlingTargetOnClick(val) {
        const res = getSpeedTime(val - 1); // 0 -4
        this.setState({
            curlingTarget: val,
            curlingRes: res,
        });
    }

    sentenceAddCase(e, index, val) { // event, index
        const a = [...this.state.sentenceClassInfo];
        a[index].cases = [...a[index].cases, val].splice(0, 50); // max sentence 50
        this.setState({
            sentenceClassInfo: a
        })
    }

    sentenceRemoveCase(i1, i2) {
        const a = [...this.state.sentenceClassInfo];
        a[i1].cases.splice(i2, 1);
        this.setState({
            sentenceClassInfo: a
        })
    }

    sentenceDescOnChange(i, text) {
        const a = [...this.state.sentenceClassInfo];
        a[i].desc = text; //todo obj assign?
        this.setState({
            sentenceClassInfo: a
        })
    }

    sentenceTrain() {
        sentenceClassifier.init();
        this.state.sentenceClassInfo.forEach((item) => {
            item.cases.forEach((sentence) => {
                sentenceClassifier.train(sentence, item.name);
            })
        })
    }

    say(m) {
        try {
            const msg = new SpeechSynthesisUtterance();
            const voices = window.speechSynthesis.getVoices();
            msg.voice = voices[10];
            msg.voiceURI = "native";
            msg.volume = 1;
            msg.rate = 1;
            msg.pitch = 0.8;
            msg.text = m;
            msg.lang = 'en-US';
            speechSynthesis.speak(msg);
        } catch {
            Message.warning('当前浏览器不支持文本朗读');
        }
    }

    rng(p) {
        return Math.random() * (p || 100);
    }

    setImageClassIndex(label) { // find index by label.
        const i = this.state.classInfo.findIndex(item=>item.name === label);
        this.setState({
            classIndex: i,
        })
        return i;
    }

    loadFeatureExtractor(tar) { //feature extractor
        this.featureExtractor = ml5.featureExtractor('MobileNet', (err, res) => {
            if (err) {
                Message.warning('模型下载失败，正在重试');
                setTimeout(()=>{
                    this.loadFeatureExtractor(tar);
                }, 3000);
            } else {
                this.loadingCb(tar);
            }
        });
        window.featureExtractor = this.featureExtractor;
    };

    wsOnChange(e) {
        // console.log(e);
        // console.log('ws event');//todo
        let typeArr = [];
        if(e.type == Blockly.Events.CREATE || e.type == Blockly.Events.DELETE) {
            typeArr = this.getCustomTypes(e);
            // console.log(typeArr);
        }
        if(e.type == Blockly.Events.CREATE) {
            typeArr.forEach((item) => {
                const tar = item.name;
                const id = item.id;
                const clsname = item.field;
                switch (tar) {
                    case "camera": {// type="camera"
                        this.handleCreate(tar);
                        break;
                    }
                    case "set_image": {
                        this.handleCreate('output_image');
                        break;
                    }
                    case "robi_move_forward":
                    case "robi_move_back":
                    case "robi_turn_left":
                    case "robi_turn_right": {
                        this.handleCreate('robi_walking');
                        break;
                    }
                    case "image_classifier": { // image classifier
                        if (this.alreadyLoaded(tar)) {
                            console.log('model loaded before');
                        } else {
                            this.state.modelLoading.add(tar);
                            this.knn = new ml5.KNNClassifier(); // handle create ic
                            window.knn = this.knn;
                            this.loadFeatureExtractor(tar);
                        }
                        window.image_classifier_on = false; // init with off
                        this.handleCreate(['indicator_imageclass', tar]);
                        break;
                    }
                    case "image_class":
                        // console.log(e.xml.outerHTML);
                        this.setState({
                            classInfo: [...this.state.classInfo, {name: clsname||'myImageClass', color: COLORS[this.state.currColor % COLORS.length], id}], //test
                            currColor: this.state.currColor + 1,
                        });
                        break;
                    case "sentence_class": {
                        // const i = this.state.sentenceClassInfo.length + 1;
                        this.setState({
                            sentenceClassInfo: [...this.state.sentenceClassInfo, {
                                name: clsname||'mySentenceClass',
                                color: COLORS[this.state.currColor % COLORS.length],
                                id,
                                desc: '',
                                caseInput: '',
                                cases: []
                            }], //test
                            currColor: this.state.currColor + 1,
                        });
                        break;
                    }
                    case "sentence_classifier": {
                        window.sentenceClassifier = sentenceClassifier;
                        this.handleCreate([tar, 'indicator_sentenceclass', 'button_sentence_train',]);
                        break;
                    }
                    case "regression": {
                        const run = () => {
                            if (!this.state.video) {
                                Message.error('请先添加摄像头模块（Media -> Camera）');
                                return;
                            }
                            // if (this.alreadyLoaded(tar)) {
                            //     console.log('model loaded before');
                            // } else {
                                this.state.modelLoading.add(tar);
                            this.loadFeatureExtractor(tar);
                            // }
                            this.regressor = this.featureExtractor.regression(this.state.video, (err, res) => {
                                if (err) {
                                    Message.error(err.message);
                                } else {
                                    console.log('video ready')
                                }
                            });
                            this.handleCreate([tar, 'indicator_regression', 'robi_race']);
                        }
                        setTimeout(run, 1000);
                        break;
                    }
                    case "speech_to_text":
                        window.startRecording = function(cb) {
                            console.log('start recording');//todo
                            window.speechCb = cb;
                            window.HZRecorder.get(function (rec) {
                                window.recorder = rec;
                                window.recorder.start();
                            });
                        };
                        // window.stopRecording= () => {
                        //     window.recorder.stopAndUpload();
                        // };
                        this.handleCreate(tar);
                        break;
                    case "dinosaur_jump":
                        this.handleCreate('dinosaur_game');
                        break;
                    case "reinforcement":
                    case "curling":
                        this.handleCreate(tar);
                        break;
                    //add a case
                    case "code_block":
                        console.log('1234');
                        break;
                    default:
                        console.log('no op');
                }
            })
        } else if (e.type == Blockly.Events.DELETE) {
            typeArr.forEach((item) => {
                const tar = item.name;
                const id = item.id;
                switch (tar) {
                    case 'image_class':
                        this.setState({
                            classInfo: this.state.classInfo.filter(info => info.id != id),
                        });
                        break;
                    case 'sentence_class':
                        this.setState({
                            sentenceClassInfo: this.state.sentenceClassInfo.filter(info => info.id != id),
                        });
                        break;
                    case 'camera':
                        this.handleDelete(tar);
                        this.closeCam && this.closeCam();
                        break;
                    case 'image_classifier':
                        this.handleDelete(['indicator_imageclass', 'image_classifier']);
                        break;
                    case 'sentence_classifier':
                        this.handleDelete(tar);
                        break;
                    case 'speech_to_text':
                        this.setState({
                            speechRes: '',
                        });
                        this.recognition = null;
                        this.handleDelete(tar);
                        break;
                    case 'regression':
                        this.handleDelete([tar, 'indicator_'+tar, 'robi_race']);
                        break;
                    case 'reinforcement':
                    case 'curling': {
                        // this.handleDelete(tar);
                        this.handleDelete([tar, 'indicator_'+tar]);
                        break;
                    }
                }
            })
        } else if (e.type == Blockly.Events.CHANGE) {
            const tar = e.name;
            if (tar === 'image_class_name' || tar === 'sentence_class_name') { // should be diff. image class, sentence class
                const aa = [
                    [...this.state.classInfo],
                    [...this.state.sentenceClassInfo],
                ];
                aa.forEach((arr) => {
                    arr.forEach((item)=>{
                        if (item.id == e.blockId) {
                            item.name = e.newValue;
                        }
                    });
                });
                this.setState({
                    classInfo: aa[0],
                    sentenceClassInfo: aa[1],
                });
            } else if (tar === 'code_block_name') {

            }
        } else if (e.type == Blockly.Events.UI) {

        }
    }

    ideSetter(name, val) {
        this.setState({[name]: val});
    }

    ideGetter(name) {
        return this.state[name];
    }

    sendToRobotOnChange(val) {
        window.sendToRobot = val;
        this.setState({
            sendToRobot: val,
        });
        if (!val) { // when turn off
            window.client && window.client.end();
            window.client = null;

            window.socket && window.socket.emit('move_stop');
            window.socket && window.socket.disconnect();
            this.setSocketStatus('');
        } else { // turn on
            if (this.state.robotId) { // connect to robot through mqtt
                // mqtt connect
                this.setSocketStatus("机器人连接中...");
                const client = mqtt.connect(
                    mqttParams.WebSocket_URL,
                    mqttParams.options,
                );
                client.on("connect", () => {
                    console.log("mqtt Connect success");
                    this.setSocketStatus("机器人已连接");
                });
                client.on("reconnect", error => {
                    this.setSocketStatus("机器人重连中...");
                    console.log("mqtt reconnecting:", error);
                });
                client.on("error", error => {
                    this.setSocketStatus("连接失败!");
                    console.log("mqtt Connect Error:", error);
                });
                window.client = client;
            } else { // connect to vector
                //flask socket connect
                this.setSocketStatus("Vector连接中...");
                window.socket = window.io.connect("http://127.0.0.1:8080");
                window.socket.on("connect", () => {
                    console.log("flask socket connected");
                    this.setSocketStatus("Vector已连接");
                });
                window.socket.on("disconnect", () => {
                    console.log("flask socket disconnected");
                    this.setSocketStatus("");
                });
            }
        }
    }
    setSocketStatus(info) {
        const el = document.getElementById('socketStatus');
        if (el) {
            el.innerHTML = info; //todo green red
        }
    }
    triggerProgress() {
        //trigger progress update of curling component after reward
        this.setState({
            triggerProgress: '',
        })
    }

    render() {
        const filename = IMAGES[this.state.classIndex] || '';
        const tabs = this.state.uiBlocks.filter(i=>TAB_NAME[i]);
        return (
            <div className="ide-wrap">
                <div className="blockly-panel">
                    <div className="ide-play-buttons">
                        <div className="button-wrap">
                            {this.state.playing ?
                                <div className="menu-button button-stop" onClick={() => {this.stop()}} />
                                : <div className="menu-button button-play" onClick={() => {this.play()}} />}
                        </div>
                        <div className="button-wrap">
                            <div className="menu-button button-demo" onClick={()=>this.demo()} />
                        </div>
                    </div>
                    <div id="blocklyDiv">
                        <div className="save-button-wrap">
                            <button className="save-button" onClick={ () => { this.save() } }>保存模块</button>
                            <button className="save-button" onClick={ () => { this.load() } }>载入模块</button>
                        </div>
                    </div>
                    <Toolbox/>
                </div>
                <div className="main-content-wrap">
                    <div className="app-top-wrap">
                        <div className="o3 app-top">
                            {this.state.params.test && <GenedCode/>}
                            {this.state.uiBlocks.map((item) => {
                                switch (item) {
                                    case 'camera':
                                        return (
                                            <InputCamera
                                                registerVideo={(v)=>this.registerVideo(v)}
                                                regCloseCam={(v)=>this.regCloseCam(v)}
                                                key={item}
                                            >
                                                <FullScreenCheck checked={this.state.fs_cam} clickHandler={()=>this.toggle('fs_cam')}/>
                                            </InputCamera>
                                        );
                                        break;
                                    case 'speech_to_text':
                                        return (
                                            <SpeechToText
                                                text={this.state.speechRes}
                                                start={()=>{
                                                    window.recorder.stopAndUpload();
                                                }}
                                                key={item}
                                            />
                                        );
                                        break;
                                    case 'output_image':
                                        return (
                                            <OutputImage srcFileName={filename} key={item}>
                                                <FullScreenCheck checked={this.state.fs_image} clickHandler={()=>this.toggle('fs_image')}/>
                                            </OutputImage>
                                        );
                                        break;
                                    case 'curling':
                                        return (
                                            <Curling
                                                target={this.state.curlingTarget}
                                                targetOnClick={(v)=>{this.curlingTargetOnClick(v)}}
                                                triggerProgress={this.state.triggerProgress}
                                                key={item}
                                            >
                                                {this.state.sendToRobot && <div className="sending-to-robot">Sending to robot...</div>}
                                            </Curling>
                                        );
                                        break;
                                    case 'dinosaur_game':
                                        return (
                                            <DinosaurGame key={item} />
                                        );
                                        break;
                                    case 'robi_walking':
                                        return (
                                            <RobiWalking key={item}>
                                                {this.state.sendToRobot && <div className="sending-to-robot">Sending to robot...</div>}
                                            </RobiWalking>
                                        );
                                        break;
                                    case 'robi_race':
                                        return (
                                            <RobiRace key={item} turn={this.state.regVal}/>
                                        );
                                        break;
                                    default:
                                        return null;
                                }
                            })}
                        </div>
                        <UICompToolBar
                            toolOnClick={(item)=>this.toolOnClick(item)}
                        />
                        {<Checkbox className="send-to-robot" checked={this.state.sendToRobot} onChange={(val)=>{this.sendToRobotOnChange(val)}}>To Robot:
                            <input value={this.state.robotId} style={{width: '70px'}}
                                   onChange={(e)=>{this.ideSetter('robotId', e.target.value);}}
                            />
                            <span id='socketStatus'></span>
                        </Checkbox>}
                    </div>
                    <div className="app-bottom-wrap">
                        {/*<div className="tab-label">Teachable Machine</div>*/}
                        <Loading className="loading-wrap" text="模型加载中" loading={this.state.modelLoading.size > 0}>
                            <div className="app-bottom-tabs-wrap">
                                {
                                    tabs.map((item, index) => {
                                        const tabName = TAB_NAME[item];
                                        return (
                                            <span className={`tab${item === this.state.currTab ? ' active' : ''}`} key={index} onClick={()=>this.ideSetter('currTab', item)}>
                                                {tabName}
                                            </span>
                                        )})
                                }
                            </div>
                            <div className="app-bottom">
                                {this.state.uiBlocks.map((item) => {
                                    // if (WHICH_TAB[item] !== this.state.currTab) return null;
                                    const show = WHICH_TAB[item] === this.state.currTab;
                                    let res = null;
                                    switch (item) {
                                        case 'image_class':
                                            return (
                                                (this.state.classInfo).map((item, index) => {
                                                    return ( // arrays wrap here, others set into res then wrap together
                                                        <div style={{display: (show ? 'block':'none')}}>
                                                            <ClassTrain
                                                                index={index}
                                                                knn={this.knn}
                                                                featureExtractor={this.featureExtractor}
                                                                video={this.state.video}
                                                                gotResults={(res)=>{this.gotResults(res)}}
                                                                classInfo={item}
                                                            />
                                                        </div>
                                                    )
                                                })
                                            );
                                            break;
                                        case 'sentence_class':
                                            return (
                                                (this.state.sentenceClassInfo).map((item, index) => {
                                                    return (
                                                        <div style={{display: (show ? 'block':'none')}}>
                                                            <SentenceClass
                                                                index={index}
                                                                classInfo={item}
                                                                addCase={(e, index, val)=>{this.sentenceAddCase(e, index, val)}}
                                                                removeCase={(i1, i2)=>{this.sentenceRemoveCase(i1, i2)}}
                                                                descOnChange={(i, t)=>this.sentenceDescOnChange(i, t)}
                                                            />
                                                        </div>
                                                    )})
                                            );
                                            break;
                                        case 'button_sentence_train':
                                            res = (
                                                <div className="button_sentence_train">
                                                    <div className="title"></div>
                                                    <Button type="primary"  size="large" onClick={()=>this.sentenceTrain()} style={{width:140}}key={item}>Train</Button>
                                                    <image></image>
                                                </div>
                                            );
                                            break;
                                        case 'regression':
                                            res = (
                                                <RegressionTrain
                                                    knn={this.regressor}
                                                    video={this.state.video}
                                                    // gotResults={(err, res)=>{this.regressionGotResults(err, res)}}
                                                    resetRes={()=>{this.regressionResetRes()}}
                                                    predict={()=>{
                                                        this.startReg();
                                                        this.regressionPredict();
                                                    }}
                                                    stopReg={()=>this.stopReg()}
                                                    predicting={this.state.regPredicting}
                                                    key={item}
                                                />
                                            );
                                            break;
                                        case 'reinforcement':
                                            res = (
                                                <ReinforcementLearning
                                                    speed={this.state.curlingRes.speed}
                                                    time={this.state.curlingRes.time}
                                                    target={this.state.curlingTarget}
                                                    curlingRes={this.state.curlingRes}
                                                    triggerProgress={()=>this.triggerProgress()}
                                                    key={item}
                                                />
                                            );
                                            break;
                                        default:
                                            const name = item.split('_');
                                            if (name[0] && name[0] === 'indicator') {
                                                if (name[1] && name[1] === 'regression') { // indicator_regression
                                                    res = (
                                                        <Indicator
                                                            title={'Regression Indicator'}
                                                            name={item}
                                                            classInfo={{
                                                                name: this.state.res,
                                                                color: '#FF6F79',
                                                            }}
                                                            key={item}
                                                        />
                                                    );
                                                } else if (name[1] && name[1] === 'imageclass') { // indicator_imageclass
                                                    res = (
                                                        <Indicator
                                                            title={'Image Class Indicator'}
                                                            name={item}
                                                            classInfo={this.state.classInfo[this.state.classIndex]}
                                                        />
                                                    )
                                                } else if (name[1] && name[1] === 'sentenceclass') { // indicator_sentenceclass
                                                    const info = this.state.sentenceClassInfo.filter(item => item.name == this.state.speechRes);
                                                    res = (
                                                        <Indicator
                                                            title={'Sentence Class Indicator'}
                                                            name={item}
                                                            classInfo={info[0] || {}}
                                                        />
                                                    )
                                                }
                                            }
                                    }
                                    return (
                                        <div style={{display: (show ? 'block':'none')}}>
                                            {res}
                                        </div>
                                    );
                                })}
                            </div>
                        </Loading>
                    </div>
                </div>
                {this.state.fullScreen && <FullScreen
                    close={()=>this.exitDemo()}
                    srcFileName={filename}
                    cam={this.state.fs_cam}
                    image={this.state.fs_image}
                />}
            </div>
        )
    }
}
