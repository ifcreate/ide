var demoWorkspace;

setTimeout(inject, 500); // 等待react脚本插入blocklyDiv.
//如果能将打包后的ide.js插入blockController.js之前（但不能在head里。react会找不到app div）,就不需此延时了。

function inject () {
    console.log('step2: inject blockly');
    demoWorkspace = Blockly.inject('blocklyDiv',
        { media: '../src/lib/blockly/media/',
            toolbox: document.getElementById('toolbox')}
    );
    demoWorkspace.addChangeListener(window.wsListener);
}

/*function wsOnChange(event) {
    if(event.type == Blockly.Events.CREATE) {
        if (event.xml.outerHTML.indexOf('camera') > -1) { // type="camera"
            console.log(event.xml.outerHTML);
        }
    } else if (event.type == Blockly.Events.DELETE) {

    } else if (event.type == Blockly.Events.CHANGE) {

    } else if (event.type == Blockly.Events.CHANGE) {

    }
}*/

// function run() {
    // var code = Blockly.JavaScript.workspaceToCode(demoWorkspace);
    // alert(code);
// }
