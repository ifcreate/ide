import mqtt from  'mqtt'

// connect options
const options = {
    connectTimeout: 4000,

    // Authentication
    clientId: 'emqx' + Date.now(), // need unique id
    username: 'if', // if
    password: 'ifcreate', // ifcreate

    keepalive: 600,
    clean: true,
};

// WebSocket connect url
// const WebSocket_URL = 'ws://mqtt.gaojulong.com:8083/mqtt';
const WebSocket_URL = 'wss://mqtt.gaojulong.com:8084/mqtt';

// TCP/TLS connect url
// const TCP_URL = 'mqtt://localhost:1883';
// const TCP_TLS_URL = 'mqtts://localhost:8883';

// const client = mqtt.connect(WebSocket_URL, options);
//
// client.on('connect', () => {
//     console.log('Connect success');
//
//     //test mqtt todo
//     console.log(client.connected);
//     client.publish('/admin/car001', '前进', (error) => {
//         console.log(error || 'Publish Success')
//     })
//
// })
//
// client.on('reconnect', (error) => {
//     console.log('reconnecting:', error)
// })
//
// client.on('error', (error) => {
//     console.log('Connect Error:', error)
// })

export default {
    options,
    WebSocket_URL,
};
