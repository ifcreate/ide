import axios from 'axios'
const base = 'http://127.0.0.1:8080';

export default {
    turn_left() { // will keep turning
        axios.post(`${base}/turn_left`);
    },
    turn_right() { // will keep turning
        axios.post(`${base}/turn_right`);
    },
    move_forward() {
        axios.post(`${base}/move_forward`);
    },
    move_stop() {
        axios.post(`${base}/move_stop`);
    },

}
