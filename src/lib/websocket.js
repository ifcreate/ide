//websocket setup

const wsApi = {
    createWebsocket(statusCb) {
        window.ws = new WebSocket('wss://neo-edu.io/websocket');
        window.wsinit = new Date();
        console.log(window.ws);
        window.ws.onopen = ()=> {
            window.wsopen = new Date();
            console.log('websocket已建立, 耗时' + Math.ceil((window.wsopen - window.wsinit)/1000) + '秒');
            statusCb && statusCb('connected');
            //为onmessage绑定事件，接收消息
            window.ws.onmessage = function (event) {
                console.log(event.data);
            }

        };
        window.ws.onclose = function() {
            window.wsclose = new Date();
            console.log('websocket已经关闭, 连接保持了' + Math.ceil((window.wsclose - window.wsopen)/1000) + '秒');
            statusCb && statusCb('...');
        };
        window.ws.onerror = function() {
            console.log('web socket connect error.');
            statusCb && statusCb('...');
        };
    },
    websocketSend(val, statusCb) {
        if (window.ws && window.ws.readyState == 1) {
            window.ws.send('' + val); // test
            console.log('ws发送：' + val);
            statusCb && statusCb('connected');
        } else if (!window.ws || window.ws.readyState >= 2) {
            this.createWebsocket(statusCb);
            console.log('重新建立websocket')
            statusCb && statusCb('connecting...');
        } else {
            console.log('websocket建立中');
            statusCb && statusCb('connecting...');
        }
    }
}

export default wsApi;
