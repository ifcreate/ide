import {index_of_max} from './utils.js';
import {random_choose} from "./utils.js";
import {gaussian} from "./utils.js";
import {set_nan} from "./utils.js";

class ActionSpace {
    // Action space is the list of [v, t]
    constructor(vmin=50, vmax=200, tmin=50, tmax=150, clip=6) {
        this.vspace = [];
        this.tspace = [];
        this.action_space = [];
        let vstep = (vmax - vmin) / (clip - 1);
        let tstep = (tmax - tmin) / (clip - 1);
        for (let i = 0; i < clip; i++) {
            this.vspace.push(vmin + i*vstep);
            this.tspace.push(tmin + i*tstep);
        }
        for (let i =0; i < this.vspace.length; i++) {
            for (let j = 0; j < this.tspace.length; j++) {
                this.action_space.push([this.vspace[i], this.tspace[j]]);
            }
        }

        // 按v+t的大小排序
        this.action_space.sort(function(a,b){
            return (a[0]+a[1]) - (b[0] + b[1]);
        });
    }


    // input: action index
    // output: [v, t]
    output_action(index) {
        let v = this.action_space[index][0];
        let t = this.action_space[index][1];
        let v_gaussian_array = [];
        let t_gaussian_array = [];
        let v_gaussian_function = gaussian(v, 3);
        let t_gaussian_function = gaussian(t, 3);

        // make a bunch of standard variates
        for(let i=0; i<2000; i++) {
            v_gaussian_array.push(v_gaussian_function());
            t_gaussian_array.push(t_gaussian_function());
        }

        return {
            v: random_choose(v_gaussian_array),
            t: random_choose(t_gaussian_array)
        }
    }
}


class QLearningTabel {
    constructor(num_state, num_actions, learning_rate=0.1, e_greedy=0.95) {
        this.num_state = num_state;
        this.num_actions = num_actions;
        this.lr = learning_rate;
        this.epsilon = e_greedy;
        this.q_table = this.init_q_table();
    }

    init_q_table() {
        let q_table = [];
        // 初始化 q_table (state x actions)
        for(let i = 0;i<this.num_state;i++){
            q_table[i] = [];
            for(let j = 0;j<this.num_actions;j++){
                q_table[i][j] = 0;
            }
        }
        return q_table;
    }

    choose_action_index(state) {
        let action_index = 0;
        let state_row = this.q_table[state];
        if (Math.random() < this.epsilon) {
            // choose best action
            action_index = index_of_max(state_row);
        }  else {
            // choose random action
            action_index = Math.floor(Math.random() * this.num_actions);
            // console.log('choose random action: ', action_index);
        }
        return action_index
    }

    choose_action_index_nonezero(state) {
        let action_index = this.choose_action_index(state);
        let state_row = this.q_table[state];
        let zero_index_arr = [];
        let zero_index = 0;

        if (this.q_table[state][action_index] !== 0) {
            // 所有Q值为0的index存入zero_index数组
            for (let i=0; i<state_row.length; i++) {
                if(state_row[i] === 0) {
                    zero_index_arr.push(i);
                }
            }
            // 随机选择一个Q值为0的index
            if (zero_index_arr.length) {
                zero_index = random_choose(zero_index_arr);
                action_index = zero_index;
            }
            // console.log('choose action Q(s,a) = 0: ', action_index);
        }
        return action_index
    }

    learn(state, action, reward) {
        let q_predict = this.q_table[state][action];
        let q_value = this.lr * (reward - q_predict);
        this.q_table[state][action] += q_value;  // update
        this.q_table[state][action] = parseFloat(this.q_table[state][action].toFixed(2));  // update
        // 把奖励值小的Q自动赋值为小的奖励
        if (reward === 1) {
            set_nan(this.q_table[state], 3, action, q_value);
        }
        if (reward === 2) {
            set_nan(this.q_table[state], 2, action, q_value);
        }
        if (reward === 3) {
            set_nan(this.q_table[state], 1, action, q_value);
        }
    }
}


export function getSpeedTime(state) {
    let action_index = Q.choose_action_index_nonezero(state);
    let v = action_space.output_action(action_index).v;
    let t = action_space.output_action(action_index).t;
    return {
        speed: parseInt(v),
        time: parseInt(t),
        index: action_index
    }
}


export function clickReward(state, action, reward) {
    Q.learn(state, action, reward);
}

export function trainingProgress(state) {
    const arr = (Q.q_table[state]).filter(i=>i!=0);
    return Math.round((arr.length / Q.q_table[state].length) * 100);
}

// export function main() {
//     let target = parseInt(prompt("请输入本次目标值 (1-5): ","0"));
//     let state = target - 1;
//     // 获取速度，时间
//     let result = getSpeedTime(state);
//     let speed = result.speed;
//     let time = result.time;
//     let action_index = result.index;
//     console.log('target:', target,  'v=:', speed,  't=:', time, 'index = ', action_index);
//     let reward = parseInt(prompt("请输入本次奖励值 (1-5): ","0"));
//     // 点击“奖励”执行，训练模型
//     clickReward(state, action_index, reward);
// }


// ---------- main --------------------------------------------------------------------------
export var action_space = new ActionSpace();
export var Q = new QLearningTabel(5,action_space.action_space.length);
//
// for(let i=0; i<5; i++) {
//     console.log('=================');
//     main();
//     console.log('=================');
// }

// ---------- test ------------------------------------------------------------------------
