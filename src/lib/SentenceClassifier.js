export default  {
    init() {
        window.classes = [];      // 有哪些类
        window.classWords = [];   // 每一类里包含哪些词
        window.corpusCount = [];  // 每个词总体出现了几次
        window.classCount = [];   // 每一类中，每个词出现了几次
    },
    sentenceToWords(sentence) {
        // 将句子处理成只含字母数字的小写

        let str = sentence.toLowerCase();
        // 去掉's
        str = str.replace(/'s/g, '');
        // 去掉标点
        str = str.replace(/[.,\/?#!$%\^&\*;:{}=\-_`~()]/g, "");
        // 提取word
        str = str.split(" ");

        return str;

    },
    train(sentence, sentenceClass) {
        // 将新类加入列表，并初始化数据结构

        if (!classes.includes(sentenceClass)) {
            classes.push(sentenceClass)
            // classWords.push({
            //     sentenceClass: []
            // });
            classWords [sentenceClass] = [];
            classCount [sentenceClass] = [];
        }

        const words = this.sentenceToWords(sentence);

        for (let i = 0; i < words.length; i++) {
            // Add this word to its corresponding class

            if (!classWords [sentenceClass].includes(words[i])) {
                // 将新词加入本类词列表
                classWords [sentenceClass].push(words[i]);
            }

            // 更新词的总体数量
            if (!corpusCount.hasOwnProperty(words[i])) {
                corpusCount [words[i]] = 1;
            } else {
                corpusCount [words [i]]++;
            }

            // 更新词在本类中的数量
            if (!classCount[sentenceClass].hasOwnProperty(words[i])) {
                classCount [sentenceClass] [words[i]] = 1;
            } else {
                classCount [sentenceClass] [words [i]]++;
            }
        }
        console.log('sentence train done!');
    },
    calculateClassScoreNaive(words, sentenceClass) {
        // 简化版的 TF-IDF
        // 如果一个词出现在了这一类里，那么得到一分基准分，否则零分
        // 在整体词库中，一个词出现次数越多，基准分被削减的就越多，分越低。
        let score = 0.0;
        for (i = 0; i < words.length; i++) {
            if (classWords [sentenceClass].includes(words[i])) {
                score += 1.0 / corpusCount[words[i]];
            }
        }
        return score;

    },
    calculateClassScoreDefault(words, sentenceClass) {
        // 考虑到了类中词频的 TF-IDF
        // 根据词出现在某类中的次数计算基准分，次数越多，基准分越高。
        // 与简化版相同，在整体词库中，一个词出现次数越多，基准分被削减的就越多，分越低。
        let score = 0.0;
        for (let i = 0; i < words.length; i++) {
            if (classWords [sentenceClass].includes(words[i])) {
                let weight = classCount [sentenceClass] [words [i]];
                score += weight / corpusCount[words[i]];
            }
        }
        return score;
    },
    calculateClassScoreTFIDF(words, sentenceClass) {
        // 标准版的 TF-IDF
        // TF 的计算与考虑词频的版本相同，根据词出现在某类中的次数计算基准分，次数越多，基准分越高
        // 但会将基准分除以该类中的总词数，来消除类之间词数不均衡带来的影响。
        // IDF 的计算将总类数除以词出现过的类数，总之就是词越是只在一类中出现，分数越高。
        // 词出现过的类数加一是为了避免除以零的情况。
        let score = 0.0;

        for (let i = 0; i < words.length; i++) {
            let tf = 0.0;
            if (classWords [sentenceClass].includes(words[i])) {
                tf = classCount [sentenceClass] [words [i]] / classWords [sentenceClass].length;
            }

            let idf = classes.length;
            let count = 0;  // 某个词在所有类中出现的次数
            for (let j = 0; j < classes.length; j++) {
                if (classWords [classes [j]].includes(words [i])) {
                    count++;
                }
            }

            idf /= 1 + count;

            score += tf * idf;
        }

        return score;
    },
    classify(sentence, selectedClassifier) {
        // 可选参数 selectedClassifier。
        // 可选值有：
        // naive	不考虑词频的最简化版本
        // default	考虑词频的版本
        // tf-idf	标准的TF-IDF算法
        // 几个版本效果基本一致，在一些特殊例子上各有不同，可以根据实际情形选择
        let maxScore = 0.0;
        let result = "None";
        const words = this.sentenceToWords(sentence);
        // JS 函数调用函数，局部变量共享 o(╥﹏╥)o
        for (let k = 0; k < classes.length; k++) {
            let score = 0.0;
            switch (selectedClassifier) {
                case "naive":
                    score = this.calculateClassScoreNaive(words, classes[k]);
                    break;
                case "tf-idf":
                    score = this.calculateClassScoreTFIDF(words, classes[k]);
                    break;
                default:
                    score = this.calculateClassScoreDefault(words, classes[k]);
                    break;
            }
            if (score > maxScore) {
                maxScore = score;
                result = classes[k];
            }
        }
        return result;
    },
}

// Train

// train("Hi! How are you?", "greet");
// train("Nice weather today, huh?", "greet");
// train("Beautiful weather!", "greet");
// train("Hello! Nice to meet you!", "greet");
//
// train("Hello! I want some milk", "milk");
// train("Hello! I would like some milk", "milk");
// train("A glass of milk, please", "milk");
// train("A glass of milk would be nice", "milk");
//
// train("I want some cola", "coke");
// train("I would like some coke", "coke");
// train("A glass of 7-up, please", "coke");
// train("A glass of coca-cola", "coke");
//
// console.log("classes: ", classes);
// console.log("classWords: ",classWords);
// console.log("corpusCount: ",corpusCount);
// console.log("classCount: ",classCount);
//
//
// // Test
//
// // "greet"
// console.log(classify("Hello, Robi! How's it going?"));
// // "milk"
// console.log(classify("Could you fetch me a cup of coca-cola?"));
// // "coke"
// console.log(classify("Could you fetch me a cup of coca-cola?"));
