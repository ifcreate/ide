const mqttUsername = 'if';
window.codeBlocks = {};
window.codeBlockIds = {};
const BNames = {
    'robi_say': 'Vector say:',
    'vector_turn': 'Vector turn',
    'vector_go': 'Vector go',
    'vector_go_at': 'Vector go at',
    'lift_arm_to':'Vector lift arm to',
    'raise_head_to':'Vector raise head to'
};

//AI
Blockly.Blocks['camera'] = {
    init: function() {
        this.setPreviousStatement(true, null);
        this.appendDummyInput()
            .appendField('Camera');
        this.setNextStatement(true, null);
        this.setColour('#FFBE42');
        this.setTooltip("");
        this.setHelpUrl("");
    },
};
Blockly.JavaScript['camera'] = function(block) {
    // String or array length.
    const code = `
    if (!window.video) {
     window.video = document.querySelector('#inputcam');
        const constraints = {
            video: true,
            audio: false
        };
        navigator.mediaDevices //todo 兼容性代码
            .getUserMedia(constraints)
            .then(stream => {
                video.srcObject = stream;
            })
            .catch(error => {
                console.error(error);
                console.log('获取摄像头出错')
            });
     }
     `;
    return code;
};

Blockly.Blocks['image_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "image_classifier",
            "message0": "Image Classifier %1 with %2 IC_Result %3",
            "args0": [
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_statement",
                    "name": "classes",
                    "check": "image_class"
                },
                {
                    "type": "input_statement",
                    "name": "res handler"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "图像分类器",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['image_classifier'] = function(block) {
    // var statements_classes = Blockly.JavaScript.statementToCode(block, 'classes');
    var statements_res_handler = Blockly.JavaScript.statementToCode(block, 'res handler');
    var code = `
    function predict () {
        if (totalCount() <= 0) {
            console.error('There is no examples in any label');
            return;
        }
        const features = featureExtractor.infer(video);
        knn.classify(features, (err, res)=> gotResults(err, res));
    };
    function gotResults(err, results) {
        if (err) {console.error(err);}
        //console.log(results.confidencesByLabel);
        if (results.classIndex < 0) {
            return;
        }
        if (1||window.image_classifier_on) { 
            const i = window.setImageClassIndex(results.label);
            IC_Result = results.label;
            ${statements_res_handler}
            if(window.sendToRobot) {
                if (window.client && window.client.connected) {
                    const s = '' + (i + 1); //publish second param need string?
                    const id = window.ideGetter('robotId');
                    const theme = '/${mqttUsername}/' + id;
                    id && window.client.publish(theme, s, (error) => {
                        console.log(error || 'Publish Success')
                    })
                } else {
                    console.log('mqtt client not connected')
                }
            }
        }
        return IC_Result;
    }
    function totalCount() {
        const map = knn.getCountByLabel();
        if (!knn || !map) return 0;
        let res = 0;
        for (var i in map) {
            res += map[i];
        }
        return res;
    }
    predict();
    `;
    return code;
};

Blockly.Blocks['sentence_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "sentence_classifier",
            "message0": "Sentence Classifier %1 with %2 SC_Result %3",
            "args0": [
                {
                    "type": "input_dummy"
                }, {
                    "type": "input_statement",
                    "name": "classes",
                    "check": "sentence_class"
                }, {
                    "type": "input_statement",
                    "name": "res handler"
                }

            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "句子分类器",
            "helpUrl": ""
        });

    },
};
Blockly.JavaScript['sentence_classifier'] = function(block) {
    var statements_res_handler = Blockly.JavaScript.statementToCode(block, 'res handler');
    var code = `
    if (1||window.sentence_classifier_on) {
        ${statements_res_handler}
    }
    `;
    return code;
};

Blockly.Blocks['regression'] = {
    init: function() {
        this.jsonInit({
            "type": "regression",
            "message0": "Regression",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['regression'] = function(block) {
    var code = ``;
    return code;
};

Blockly.Blocks['speech_to_text'] = {
    init: function() {
        this.jsonInit({
            "type": 'speech_to_text',
            "message0": "Speech to Text %1 %2",
            "args0": [
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_statement",
                    "name": "res handler"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['speech_to_text'] = function(block) {
    var statements_res_handler = Blockly.JavaScript.statementToCode(block, 'res handler');
    var code = `
    sttStartCount && sttStartCount(); 
    window.startRecording(()=>{${statements_res_handler}});
    window.ideSetter && window.ideSetter('speechRes', 'Recording...');
    // window.recognition.onresult = (event) => {
        //     window.ideSetter && window.ideSetter('speechRes', text);
        //     SC_Result = window.sentenceClassifier && window.sentenceClassifier.classify(text);
        //
        // };
    
    `;
    return code;
};

Blockly.Blocks['reinforcement'] = {
    init: function() {
        this.jsonInit({
            "type": "reinforcement",
            "message0": "Reinforcement Learning",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['reinforcement'] = function(block) {
    var code = ``;
    return code;
};

Blockly.Blocks['curling'] = {
    init: function() {
        this.jsonInit({
            "type": "curling",
            "message0": "Curling Target",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['curling'] = function(block) {
    var code = ``;
    return code;
};

Blockly.Blocks['image_class'] = {
    init: function() {
        this.jsonInit({
            "type": "image_class",
            // "message0": "image class: %1 Indicators %2",
            "message0": "image class: %1",
            "args0": [
                {
                    "type": "field_input",
                    "name": "image_class_name",
                    "text": "myImageClass"
                },
                // {
                //     "type": "field_dropdown",
                //     "name": "indicator",
                //     "options": [
                //         [
                //             "Default",
                //             "opDefault",
                //         ]
                //     ]
                // },
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#00C3B3',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['image_class'] = function(block) {
    var text_class = block.getFieldValue('class');
    var dropdown_indicator = block.getFieldValue('indicator');
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = '\n';
    return code;
};

Blockly.Blocks['sentence_class'] = {
    init: function() {
        this.jsonInit({
            "type": "sentence_class",
            // "message0": "class: %1 Indicators %2",
            "message0": "sentence class: %1",
            "args0": [
                {
                    "type": "field_input",
                    "name": "sentence_class_name",
                    "text": "mySentenceClass"
                },
                // {
                //     "type": "field_dropdown",
                //     "name": "indicator",
                //     "options": [
                //         [
                //             "Default",
                //             "opDefault",
                //         ]
                //     ]
                // },
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#00C3B3',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['sentence_class'] = function(block) {
    var text_class = block.getFieldValue('class');
    var dropdown_indicator = block.getFieldValue('indicator');
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = '\n';
    return code;
};

Blockly.Blocks['set_image'] = {
    init: function() {
        this.jsonInit({
            "type": "set_image",
            "message0": "Set %1 to %2",
            "args0": [
                {
                    "type": "field_dropdown",
                    "name": "block type",
                    "options": [
                        [
                            "Image",
                            "opimage"
                        ]
                    ]
                },
                {
                    "type": "field_dropdown",
                    "name": "file name",
                    "options": [
                        [
                            "rock.png",
                            "rock.png"
                        ],
                        [
                            "paper.png",
                            "paper.png",
                        ],
                        [
                            "scissors.png",
                            "scissors.png",
                        ],
                        [
                            "leftHand.png",
                            "leftHand.png",
                        ],
                        [
                            "rightHand.png",
                            "rightHand.png",
                        ],
                        [
                            "bothHands.png",
                            "bothHands.png",
                        ]
                    ]
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#0391F2',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['set_image'] = function(block) {
    const code = `
    var outputImage = document.querySelector('.output-image');
    var fs_image = document.querySelector('.img-face');
    if (outputImage) {
        outputImage.src = '../src/images/${block.getFieldValue('file name')}';
    }
    if (fs_image) {
        fs_image.src = '../src/images/${block.getFieldValue('file name')}';
    }
    `;
    return code;
};

Blockly.Blocks['set_res'] = {
    init: function () {
        this.jsonInit({
                "type": "set_res",
                "message0": "set %1 %2 = %3",
                "args0": [
                    {
                        "type": "input_dummy"
                    },
                    {
                        "type": "field_input",
                        "name": "res",
                        "text": "result"
                    },
                    {
                        "type": "input_value",
                        "name": "var name"
                    }
                ],
                "inputsInline": true,
                "previousStatement": null,
                "nextStatement": null,
                "colour": 230,
                "tooltip": "",
                "helpUrl": ""
            }
        )
    }
};
Blockly.JavaScript['set_res'] = function(block) {
    var text_res = block.getFieldValue('res');
    var value_var_name = Blockly.JavaScript.valueToCode(block, 'var name', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = `var ${text_res} = resObj.res; \n`;
    return code;
};

//Control
/* Blockly.Blocks['start_image_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "start_image_classifier",
            "message0": "start image classifier",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['start_image_classifier'] = function(block) {
    var code = `window.image_classifier_on = true;
    `;
    return code;
};

Blockly.Blocks['stop_image_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "stop_image_classifier",
            "message0": "stop image classifier",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['stop_image_classifier'] = function(block) {
    var code = `window.image_classifier_on = false;
    `;
    return code;
};

Blockly.Blocks['start_sentence_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "start_sentence_classifier",
            "message0": "start sentence classifier",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['start_sentence_classifier'] = function(block) {
    var code = `window.sentence_classifier_on = true;
    `;
    return code;
};

Blockly.Blocks['stop_sentence_classifier'] = {
    init: function() {
        this.jsonInit({
            "type": "stop_sentence_classifier",
            "message0": "stop sentence classifier",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['stop_sentence_classifier'] = function(block) {
    var code = `window.sentence_classifier_on = false;
    `;
    return code;
}; */

//Media

//simulator
Blockly.Blocks['dinosaur_jump'] = {
    init: function() {
        this.jsonInit({
            "type": "dinosaur_jump",
            "message0": "Dinosaur Jump",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['dinosaur_jump'] = function(block) {
    var code = `
    if (dinosaurLastState === 'duck') { //release duck
        var e = new Event('keyup');
        e.keyCode='40';
        e.target = document.body;
        document.dispatchEvent(e);
    }
    var e = new Event('keydown');
    e.keyCode = '32'; // space
    e.target = document.body;
    document.dispatchEvent(e);
    window.dinosaurLastState = 'jump';`;
    return code;
};

Blockly.Blocks['dinosaur_duck'] = {
    init: function() {
        this.jsonInit({
            "type": "dinosaur_duck",
            "message0": "Dinosaur Duck",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['dinosaur_duck'] = function(block) {
    var code = `
    if (dinosaurLastState === 'jump') { //release jump
        var e = new Event('keyup');
        e.keyCode='32';
        e.target = document.body;
        document.dispatchEvent(e);
    }
    var e = new Event('keydown');
    e.keyCode = '40'; //down
    e.target = document.body;
    document.dispatchEvent(e);
    window.dinosaurLastState = 'duck';`;
    return code;
};

Blockly.Blocks['dinosaur_stand'] = {
    init: function() {
        this.jsonInit({
            "type": "dinosaur_stand",
            "message0": "Dinosaur Stand",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['dinosaur_stand'] = function(block) {
    var code = `
    if (dinosaurLastState != 'stand') {
        var e = new Event('keyup');
        if (dinosaurLastState == 'jump') {
            e.keyCode = '32';
        } else if (dinosaurLastState == 'duck') {
            e.keyCode='40';
        }
        e.target = document.body;
        document.dispatchEvent(e);
    }
    window.dinosaurLastState = 'stand';
    `;
    return code;
};

//robi walking
Blockly.Blocks['robi_turn_left'] = {
    init: function() {
        this.jsonInit({
            "type": "robi_turn_left",
            "message0": "Robi Turn Left",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['robi_turn_left'] = function(block) {
    var code = `window.robi_turn_left && window.robi_turn_left();
    `;
    const el = document.querySelector('robi-walking-map');
    console.log();
    return code;
};

Blockly.Blocks['robi_turn_right'] = {
    init: function() {
        this.jsonInit({
            "type": "robi_turn_right",
            "message0": "Robi Turn Right",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['robi_turn_right'] = function(block) {
    var code = `window.robi_turn_right && window.robi_turn_right();
    `;
    return code;
};

Blockly.Blocks['robi_move_forward'] = {
    init: function() {
        this.jsonInit({
            "type": "robi_move_forward",
            "message0": "Robi Move Forward",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['robi_move_forward'] = function(block) {
    var code = `window.robi_move_forward && window.robi_move_forward();
    `;
    return code;
};

Blockly.Blocks['robi_move_back'] = {
    init: function() {
        this.jsonInit({
            "type": "robi_move_back",
            "message0": "Robi Move Back",
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['robi_move_back'] = function(block) {
    var code = `window.robi_move_back && window.robi_move_back();
    `;
    return code;
};

Blockly.Blocks['robi_say'] = {
    init: function() {
        this.jsonInit({
            "type": "block_type",
            "message0": `${BNames['robi_say']} %1`,
            "args0": [
                {
                    "type": "input_value",
                    "name": "robi_say",
                    "align": "RIGHT"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['robi_say'] = function(block) {
    var value_robi_say = Blockly.JavaScript.valueToCode(block, 'robi_say', Blockly.JavaScript.ORDER_ATOMIC);
    var code = `action = 'say-'+${value_robi_say};
    window.runLogAdd && window.runLogAdd(action);
    if(window.sendToRobot) {
        const id = window.ideGetter('robotId');
        if (id) {
            if (window.client && window.client.connected) {
                const theme = '/${mqttUsername}/' + id;
                window.client.publish(theme, action, (error) => {
                    console.log(error || 'Publish Success')
                })
            } else {
                console.log('mqtt client not connected')
            }
        } else {
            window.socket && window.socket.emit('sayText', ${value_robi_say});
        }
    }`;
    return code;
};
//Math

//UI

//vector
Blockly.Blocks['vector_go_at'] = {
    init: function() {
        this.jsonInit({
            "type": "vector_go_at",
            "message0": `${BNames['vector_go_at']} %1 mm/s`,
            "args0": [
                {
                    "type": "field_number",
                    "name": "speed",
                    "value": 50,
                    "min": -220,
                    "max": 220
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['vector_go_at'] = function(block) {
    var number_speed = block.getFieldValue('speed');
    var code = ` window.socket.emit('go_at_speed', ${number_speed});`;
    return code;
};

Blockly.Blocks['vector_go'] = {
    init: function() {
        this.jsonInit({
            "type": "vector_go",
            "message0": `${BNames['vector_go']} %1 mm`,
            "args0": [
                {
                    "type": "field_number",
                    "name": "speed",
                    "value": 100,
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['vector_go'] = function(block) {
    var number_speed = block.getFieldValue('speed');
    var code = `window.socket.emit('drive_straight', ${number_speed}, 50);`;
    return code;
};

Blockly.Blocks['vector_turn'] = {
    init: function() {
        this.jsonInit({
            "type": "vector_turn",
            "message0": `${BNames['vector_turn']} %1 deg`,
            "args0": [
                {
                    "type": "field_number",
                    "name": "speed",
                    "value": 90,
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['vector_turn'] = function(block) {
    var number_speed = block.getFieldValue('speed');
    var code = `window.socket.emit('turn', ${number_speed});`;
    return code;
};

Blockly.Blocks['lift_arm_to'] = {
    init: function() {
        this.jsonInit({
            "type": "lift_arm_to",
            "message0": `${BNames['lift_arm_to']} %1 mm`,
            "args0": [
                {
                    "type": "field_number",
                    "name": "speed",
                    "value": 92,
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['lift_arm_to'] = function(block) {
    var number_speed = block.getFieldValue('speed');
    var code = `window.socket.emit('lift_arm_to', ${number_speed});`;
    return code;
};

Blockly.Blocks['raise_head_to'] = {
    init: function() {
        this.jsonInit({
            "type": "raise_head_to",
            "message0": `${BNames['raise_head_to']} %1 deg`,
            "args0": [
                {
                    "type": "field_number",
                    "name": "speed",
                    "value": 45,
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['raise_head_to'] = function(block) {
    var number_speed = block.getFieldValue('speed');
    var code = `window.socket.emit('raise_head_to', ${number_speed});`;
    return code;
};

//code block
Blockly.Blocks['code_block'] = {
    init: function() {
        this.jsonInit({
            "type": "code_block",
            "message0": "Code block %1 %2 %3 can repeat %4 %5 every %6 ms %7 %8",
            "args0": [
                {
                    "type": "input_dummy"
                },
                {
                    "type": "field_input",
                    "name": "code_block_name",
                    "text": "block1"
                },
                {
                    "type": "input_dummy"
                },
                {
                    "type": "field_checkbox",
                    "name": "can_repeat",
                    "checked": true
                },
                {
                    "type": "input_dummy"
                },
                {
                    "type": "field_number",
                    "name": "every_ms",
                    "value": 200
                },
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_statement",
                    "name": "sta_input"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['code_block'] = function(block) {
    var code_block_name = block.getFieldValue('code_block_name');
    var checkbox_can_repeat = block.getFieldValue('can_repeat') == 'TRUE';
    var number_every_ms = block.getFieldValue('every_ms');
    var statements_sta_input = Blockly.JavaScript.statementToCode(block, 'sta_input');
    // statements_sta_input = statements_sta_input.replace(/[\r\n]/g,"");
    var code = '';
    if (checkbox_can_repeat) {
            var f = function () {
                var n = setInterval(function () {
                    Function(statements_sta_input)();
                }, number_every_ms);
                window.setCodeBlockId(code_block_name, n);
            };
            console.log('block def');//todo
        window.setCodeBlock(code_block_name, f);
        code = ``;
    } else {
        var f = function () {
            Function(statements_sta_input)();
        }
        window.setCodeBlock(code_block_name, f);
        code = '';
    }
    return code;
};
Blockly.Blocks['start_code_block'] = {
    init: function() {
        this.jsonInit({
            "type": "start_code_block",
            "message0": "start code block %1",
            "args0": [
                {
                    "type": "field_input",
                    "name": "NAME",
                    "text": "block1"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['start_code_block'] = function(block) {
    var text_name = block.getFieldValue('NAME');
    // var code = `window.codeBlocks['${text_name}']`;
    var code = `window.codeBlocks['${text_name}']&&window.codeBlocks['${text_name}']();`;
    return code;
};
Blockly.Blocks['stop_code_block'] = {
    init: function() {
        this.jsonInit({
            "type": "stop_code_block",
            "message0": "stop code block %1",
            "args0": [
                {
                    "type": "field_input",
                    "name": "NAME",
                    "text": "block1"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};
Blockly.JavaScript['stop_code_block'] = function(block) {
    var text_name = block.getFieldValue('NAME');
    var code = `clearInterval(window.codeBlockIds['${text_name}']);`;
    return code;
};

function setCodeBlock(name, f) {
    window.codeBlocks[name] = f;
}
function setCodeBlockId(name, id) {
    window.codeBlockIds[name] = id;
}

Blockly.Blocks['vector_face_rec'] = {
    init: function() {
        this.jsonInit({
            "type": "vector_face_rec",
            "message0": "Vector Face Recognition %1 Result %2",
            "args0": [
                {
                    "type": "input_dummy"
                }, {
                    "type": "input_statement",
                    "name": "res handler"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": '#FFBE42',
            "tooltip": "Vector人脸识别",
            "helpUrl": ""

        });
    },
};
Blockly.JavaScript['vector_face_rec'] = function(block) {
    var statements_res_handler = Blockly.JavaScript.statementToCode(block, 'res handler');
    var code = `window.socket && window.socket.emit('face_rec');
    window.socket && window.socket.on('face', (val) => {
        console.log(1,val);
        window.face = val;
        ${statements_res_handler}
    })
    `;
    return code;
};
