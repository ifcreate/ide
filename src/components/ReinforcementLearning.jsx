import React, {Component} from 'react';
import ReinforceOutput from 'components/ReinforceOutput';
import Rewards from 'components/Rewards';
import Scores from 'components/Scores';
import 'style/reinforcement-learning.less';
import {clickReward,} from 'lib/CurlingRobot';

const V_MIN = 50;
const V_MAX = 200;
const T_MIN = 50;
const T_MAX = 150;
const T_PER_SEC = 130; // amount per second

export default class ReinforcementLearning extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scores: [0,0,0,0],
            rewardDisable: true,
        };
        this.originStoneLeft = 50;
    }
    rewardHandler(val) {
        this.setState({
            scores: [...this.state.scores, val].splice(-10), // most recent 10 records
        });
        const el = document.querySelector('.curling-stone');
        el && (el.style.left = this.originStoneLeft + 'px');
        // feedback score and speed to algo
        clickReward(this.props.target - 1, this.props.curlingRes.index, val);
        // if (arr.length == 0) {
        //     alert(`目标${this.props.target} 训练完成！现在，点击此目标后输出的速度、时间会趋向你训练时打分最高的组合了，不过有5%的失误率哦~`)
        // }
    }
    start() {
        const s = this.props.speed;
        const t = this.props.time;
        if (window.sendToRobot) {
            if (window.socket.connected) {
                const d = parseInt(s * t / T_PER_SEC); //s50~200, t50~150 (/50= 1s ~ 3s)
                window.socket.emit('drive_straight', d, s); //vector
            } else {
                window.socket = window.io.connect('http://127.0.0.1:8080');
                window.socket.on('connect', () => {
                    window.socket.send('flask socket connected2');
                })
            }
        } else { // simulator
            const el = document.querySelector('.curling-stone');
            const pos =Math.round((s * t/50 - 50) * 0.8 + 20);  // [50, 600] to [20, 450];
            if (el) {
                el.style.left = '50px';
                el.style.transition = 'left 0s';
                setTimeout(() => {
                    el.style.transition = 'left 2s';
                    el.style.left = this.originStoneLeft + pos + 'px';
                }, 0)
            }
        }
    }
    enableReward() {
        this.setState({
            rewardDisable: false,
        })
    }
    disableReward() {
        this.setState({
            rewardDisable: true,
        })
    }
    render() {
        const {speed, time} = this.props;
        return (
            <div className="reinforce-wrap ui-block bottom-block">
                <ReinforceOutput
                    speed={Math.max(5,(speed - V_MIN)) * (100 / (V_MAX - V_MIN))}
                    time={Math.max(5, (time - T_MIN)) * (100 / (T_MAX - T_MIN))}
                    start={()=>this.start()}
                    enableReward={()=>{this.enableReward()}}
                />
                <Rewards
                    rewardHandler={(v)=>this.rewardHandler(v)}
                    disableReward={()=>this.disableReward()}
                    rewardDisable={this.state.rewardDisable}
                    triggerProgress={()=>this.props.triggerProgress()}
                />
                <Scores
                    scores={this.state.scores}
                />
            </div>
        );
    }
}
