import React, { useState, useEffect } from 'react';
import 'style/tm-class-train.less';

const BW = 10;
const BH = 6;

export default function ClassTrain(props) {
    const [count, setCount] = useState(0); //0: init val
    let canvasRef = React.createRef();
    const video = props.video;
    useEffect(() => {
        draw_borders();
    }, []);
    function draw_borders() {
        const ctx = canvasRef.current.getContext('2d');
        ctx.lineWidth = 1.5;
        ctx.strokeStyle = "#d9e6ee";
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                //ctx.strokeRect(0 + i * 100 +2, 0 + j * 50+1, 100 - BM, 50 - BM);
                ctx.strokeRect(i * 100, j * 50, 100 - BW, 50 - BH);
            }
        }
        // ctx.strokeRect(100,0,100,50);
        // ctx.strokeRect(200,0,100,50);
    }
    function train() {
        // console.log('train');
        const ctx = canvasRef.current.getContext('2d');
        const index = count % 9; // thumbnail index
        const y = Math.floor(index / 3);
        const x = index % 3;

        ctx.save();
        ctx.scale(-1, 1); //mirror
        ctx.drawImage(video, -(x * 100 + 1), y * 50, -(100 - 11), 50 - 7);
        ctx.restore();
        const features = props.featureExtractor.infer(video);
        // props.knn.addExample(features, props.index+'');
        props.knn.addExample(features, props.classInfo.name); //label
        updateCount();
    };
    function clearClass(label) {
        props.knn.clearLabel(label);
    };
    function reset() {
        const canvas = canvasRef.current;
        canvas.height = canvas.height; //clear canvas
        draw_borders();
        clearClass(props.classInfo.name);
        updateCount();
    }
    const updateCount = () => {
        setCount(props.knn && props.knn.getCountByLabel()[props.classInfo.name] || 0);
    };
    return (
        <div className="class-train-wrap ui-block bottom-block">
            <div className="class-title-line">
                <div className="class-title">
                    {props.classInfo.name}
                </div>
                <span className="reset-wrap" onClick={reset}>
                    Reset
                </span>
            </div>
            <div className="canvas-wrap">
                <canvas id={`canvas${props.index}`} className="canvas-training" ref={canvasRef} />
            </div>
            <div className="class-sample-count">Samples: {count}</div>
            <div className="class-button-train" onClick={train} style={{ backgroundColor: props.classInfo.color }}>Train</div>
        </div>
    );
}
