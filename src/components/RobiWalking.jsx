import React from 'react';
import {Button} from 'element-react';
import 'style/robi-walking.less';

const MOVE_RATE = 4;
const MAP_RADIUS = 440;
const VIEWPORT_RADIUS = 150;
const WALKABLE_RADIUS = 215; // 430 / 2
const TAR_RADIUS = 15; // target radius

// 地图起始与视窗同心（通过绝对定位）x,y为在此基础上的偏移量
// tx, ty为相对于地图左上角的绝对定位
const INIT_STATE = {
    deg: 0, // robi direction
    x: 0,
    y: 0,
    tx: MAP_RADIUS + 0, // center + 100
    ty: MAP_RADIUS - 100,
    tpx: VIEWPORT_RADIUS/2,
    tpy: 0,
    tpdeg: 0,
    showtp: false, // show target pointer
};

export default class RobiWalking extends React.Component {
    constructor(props) {
        super(props);
        this.state = INIT_STATE;
        window.robi_turn_left = this.robi_turn_left.bind(this);
        window.robi_turn_right = this.robi_turn_right.bind(this);
        window.robi_move_forward = this.robi_move_forward.bind(this);
        window.robi_move_back = this.robi_move_back.bind(this);
    }
    cap (res) {
        return res > WALKABLE_RADIUS ? WALKABLE_RADIUS : res < - WALKABLE_RADIUS ? - WALKABLE_RADIUS : res;
    }
    checkTar() {
        const {x, y, tx, ty} = this.state;
        // 都转到背景图坐标系。即整张背景图左上角（0，0)
        // 地图与target相对不变，滑动视窗
        const p0 = {
            x: -x + 440,
            y: -y + 440,
        };
        const pt = { // point target
            x: tx + TAR_RADIUS,
            y: ty + TAR_RADIUS,
        };
        let d = Math.sqrt(
            Math.pow((p0.x - pt.x), 2) + Math.pow((p0.y - pt.y), 2)
        );
        d = Math.round(d);
        // console.log('p0x'+p0.x+' p0y'+p0.y+' ptx'+ pt.x+' pty'+ pt.y+' d'+ d); //todo
        if (d < TAR_RADIUS * 2) {
            this.setState({
                tx: this.getNewTargetCoord(),
                ty: this.getNewTargetCoord(),
            });
        }
        let tpdeg = 0;
        if (p0.y == pt.y) {
            if (p0.x < pt.x) {
                tpdeg = 90;
            } else {
                tpdeg = -90;
            }
        } else if (pt.y < p0.y) {
            tpdeg = - Math.atan((pt.x - p0.x)/(pt.y - p0.y)) * 57 // 180/3.14
        } else {
            tpdeg = 180 - Math.atan((pt.x - p0.x) / (pt.y - p0.y)) * 57
        }
        tpdeg = Math.round(tpdeg);
        // console.log(tpdeg+'˚');//todo
        this.setState({
            showtp: !(this.inViewPort(pt.x - p0.x) && this.inViewPort(pt.y - p0.y)),
            tpdeg: tpdeg,
        })
    }
    inViewPort(p) {
        let abs = Math.abs(p);
        return abs <= VIEWPORT_RADIUS;
    }
    getNewTargetCoord() {
        return Math.round(
            Math.random() * (WALKABLE_RADIUS*2 - TAR_RADIUS*4) +
            MAP_RADIUS - WALKABLE_RADIUS + TAR_RADIUS*2
        );
    }
    reset () {
        this.setState(INIT_STATE);
    }
    robi_move_forward() { //up
        if (window.sendToRobot) {
            window.sendToRobot && window.socket && window.socket.emit('move_forward');
        } else { // simulator
            this.setState({
                deg: 0,
                y: this.cap(this.state.y + MOVE_RATE), // * Math.cos(this.state.deg * Math.PI/180)),
            });
            this.checkTar();
        }
    }
    robi_move_back() { //down
        if (window.sendToRobot) {
            window.socket && window.socket.emit('move_stop');
        } else {
            this.setState({
                deg: 180,
                y: this.cap(this.state.y - MOVE_RATE),
            });
        }
        this.checkTar();
    }
    robi_turn_left() {
        if (window.sendToRobot) {
            window.sendToRobot && window.socket && window.socket.emit('turn_left');
        } else { // simulator
            this.setState({
                deg: -90,
                x: this.cap(this.state.x + MOVE_RATE),
            });
        }
        this.checkTar();
    }
    robi_turn_right() {
        if (window.sendToRobot) {
            window.socket && window.socket.emit('turn_right');
        } else { // simulator
            this.setState({
                deg: 90,
                x: this.cap(this.state.x - MOVE_RATE),
            });
        }
        this.checkTar();
    }
    render() {
        const {x, y, deg, tpdeg, showtp} = this.state;
        return (
            <div className="robi-walking-block ui-block top-block">
                {/*<div className="block-title">Robi Walking {props.children}</div>*/}
                <div className="block-title">Robi Walking</div>
                <Button size="mini" type="primary" onClick={()=>this.reset()}>Reset</Button>
                <Button size="mini" type="primary" onClick={()=>this.robi_turn_left()}>←</Button>
                <Button size="mini" type="primary" onClick={()=>this.robi_move_forward()}>↑</Button>
                <Button size="mini" type="primary" onClick={()=>this.robi_move_back()}>↓</Button>
                <Button size="mini" type="primary" onClick={()=>this.robi_turn_right()}>→</Button>
                <div className="map-viewport">
                    <div className="image-wrap" style={{
                        transform: `translate(${x}px, ${y}px)`,
                    }}>
                        <img className="robi-walking-map output-image"
                             src={`../src/images/${this.props.srcFileName || 'robi_walking_bg.png'}`}
                        />
                        {<div className="robi-target" style={{
                            left: `${this.state.tx}px`,
                            top: `${this.state.ty}px`,
                        }}/>}

                    </div>
                    <img className="curling-stone"
                         src={`../src/images/robi-from-top.png`}
                         style={{
                             transform: `rotate(${deg % 360}deg)`,
                         }}
                    />
                    <div className="target-pointer"
                         style={{
                             transform: `translate(${(VIEWPORT_RADIUS - 30) * Math.sin(tpdeg/57)}px, ${ - (VIEWPORT_RADIUS - 30) * Math.cos(tpdeg/57)}px) rotate(${tpdeg % 360}deg) scaleX(1.5)`,
                             visibility: showtp ? 'visible' : 'hidden',
                         }}
                    >↑</div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
