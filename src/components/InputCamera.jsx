import React, {Component} from 'react';
import 'style/cam.less';
import {Message} from 'element-react';

export default class InputCamera extends Component {
    constructor(props) {
        super(props);
        // this.videoRef = React.createRef(); // document.getElementById('inputcam'); // todo createRefs
    }
    componentDidMount() {
        // if (this.props.isApp) {
            this.getCam();
        // }
    }
    componentWillUnmount() {
        this.closeCam();
        this.props.registerVideo(null);
    }
    getCam() {
        // const video = this.videoRef.current;
        this.video = document.getElementById('inputcam');
        this.constraints = {
            video: true,
            audio: false
        };
        navigator.mediaDevices //todo 兼容性代码
            .getUserMedia(this.constraints)
            .then(stream => {
                this.video.srcObject = stream;
                this.props.registerVideo(this.video);
                this.mediaStreamTrack = stream.getVideoTracks()[0];
                this.props.regCloseCam && this.props.regCloseCam(this.closeCam.bind(this));
            })
            .catch(error => {
                console.error(error);
                console.log('获取摄像头出错');
                Message.error("获取摄像头出错。请检查您的摄像头是否工作正常");
            });
    }
    closeCam() {
        this.mediaStreamTrack && this.mediaStreamTrack.stop();
    }
    render() {
        return (
            <div className="cam-block ui-block top-block">
                <div className="block-title">Camera {this.props.children}</div>
                <div className="video-wrap">
                    <video id="inputcam" width="227" height="227" autoPlay ref={this.videoRef} />
                </div>
            </div>
        );
    }
}
