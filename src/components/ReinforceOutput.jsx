import React from 'react';
import {Progress, Button} from 'element-react';
import 'style/reinforcement-output.less';

export default function ReinforceOutput (props) {
    return (
        <div className="box-wrap rf-output-wrap">
            <div className="title">输出</div>
            <div className="line">
                <div className="label">推行速度</div>
                <div className="progress-wrap">
                    <Progress percentage={props.speed} strokeWidth={20} textInside showText={false}/>
                </div>
            </div>
            <div className="line">
                <div className="label">推行时间</div>
                <div className="progress-wrap">
                    <Progress
                        className="bar-green"
                        percentage={props.time} strokeWidth={20} textInside showText={false}/>
                </div>
            </div>
            <div className="button-line-center">
                <Button
                    type="success" size="small"
                    onClick={()=>{
                        props.enableReward();
                        props.start();
                    }}
                >执行</Button>
            </div>
        </div>
    );
}

