import React, {useState} from 'react';
import {Slider, Button} from 'element-react';
import 'style/rewards.less';

export default function Rewards (props) {
    const [value, setValue] = useState(5);
    return (
        <div className="box-wrap rewards-wrap">
            <div className="title">奖赏</div>
            <div className="line">
                <div className="slider-value">{value}</div>
                <Slider value={value} onChange={(val)=>{setValue(val)}} min={1} max={5}/>
            </div>
            <div className="button-line-center">
                <Button type="danger" size="small"
                        onClick={()=>{
                            props.rewardHandler(value);
                            props.disableReward();
                            props.triggerProgress();
                        }}
                        disabled={props.rewardDisable}
                >奖赏</Button>
            </div>
        </div>
    );
}
