import React, {Component} from 'react';
import 'style/cam.less'

export default function GenedCode (props) {
    return (
        <div className="cam-block">
            <div className="block-title">Code</div>
            <div className="video-wrap">
                <textarea name="codearea" id="codearea" cols="30" rows="20" className="codearea"></textarea>
            </div>
        </div>
    );
}
