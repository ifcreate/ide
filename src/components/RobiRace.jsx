import React, {useState, useEffect} from 'react';
import {Button} from 'element-react';
import 'style/robi-race.less'
import axios from 'axios';

export default class RobiRace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            deg: 0,
            score: 0,
            pause: false,
            countDown: GAME_TIME,
            x: 135, // middle of track,
        }
        this.audio = document.getElementById('audio-scoring');
    }
    componentDidMount() {
        this.start();
    }
    componentWillUnmount() {
        clearInterval(this.rotate);
        clearInterval(this.timer);
    }
    start() {
        this.rotate = setInterval(()=>{
            let robiDeg = this.props.turn * 1.2 - 60; //  _\|/_ to use cosine
            // let x = Math.floor(this.state.x + robiDeg/5);
            let x = Math.floor(this.state.x + (X_SPEED * Math.sin(robiDeg * 3.14/180)));
            x = Math.max(0, Math.min(265, x));
            if (this.state.pause) return;
            const score = this.getScore(this.state.deg);
            this.setState({
                deg: (this.state.deg - TURN_RATE)%360,
                score,
                x,
            })
        }, 40);
        this.timer = setInterval(()=>{
            if (this.state.pause) return;
            if (this.state.countDown === 0) {
                this.setState({
                    pause: true
                })
            } else {
                this.setState({
                    countDown: this.state.countDown - 1,
                })
            }
        }, 1000);
    }
    reset() {
        clearInterval(this.rotate);
        clearInterval(this.timer);
        this.setState({
            deg: Math.floor(-Math.random()*360),
            score: 0,
            pause: false,
            countDown: GAME_TIME,
            x: 135,
        });
        this.start();
    }
    getScore(deg) {
        const x = GATES[deg];
        if (!x) return this.state.score;
        const robiLeft = this.state.x;
        const robiRight = this.state.x + 40;
        if (x.x1 <= robiLeft && robiRight <= x.x2) {
            // console.log(x.x1+' '+this.state.x+' '+x.x2 + ' deg'+deg);//todo
            this.playSound();
            return this.state.score + 10;
        } else {
            // console.log(x.x1+' '+this.state.x+' '+x.x2+' Fail！' + 'deg'+deg);//todo
        }
        return this.state.score;
    }
    playSound() {
        this.audio && this.audio.play();
    }
    render() {
        let robiDeg = this.props.turn * 1.2 + (-60); // 0-100 map to -60 to 60
        return (
            <div className="robi-race-block ui-block top-block">
                <div className="block-title">Robi Race {this.props.children}</div>
                <div className="game-info">
                    <Button size="mini" type="primary" onClick={()=>this.reset()}>Restart</Button>
                    <b> Time Left: {this.state.countDown} </b>
                    <span> Score: {this.state.score} </span>
                </div>
                <div className="map-viewport">
                    <div className="image-wrap" style={{
                        transform: `rotate(${this.state.deg}deg)`,
                    }}>
                        <img className="robi-walking-map output-image"
                             src={`../src/images/${this.props.srcFileName || 'robi_race_circle.png'}`}
                        />
                    </div>
                    {this.state.pause && <b className="final-score">Your Score: {this.state.score}</b>}
                    <img className="curling-stone"
                         style={{
                             transform: `rotate(${robiDeg}deg)`,
                             left: `${this.state.x}px`
                         }}
                         src={`../src/images/robi-from-top.png`}
                    />
                </div>
                {/*<audio id="audio-scoring" src="http://data.huiyi8.com/2017/gha/03/17/1702.mp3"></audio>*/}
            </div>
        )
    }
}

const X_SPEED = 15;
const GAME_TIME = 60;
const TURN_RATE = 0.5;
const MOVE_RATE = 2;
const MAP_RADIUS = 800;
const GATES = {
    '-23': {x2: (420 - 248)*1.5, x1: (420 - 336)*1.5,}, // origin coord. on the 880map
    '-57': {x2: (420 - 325)*1.5, x1: (420 - 407)*1.5,},
    '-83': {x2: (420 - 302)*1.5, x1: (420 - 383)*1.5,},
    '-118':{x2: (420 - 320)*1.5, x1: (420 - 393)*1.5,},
    '-146': {x2: (420 - 289)*1.5, x1: (420 - 356)*1.5,},
    '-173': {x2: (420 - 294)*1.5, x1: (420 - 374)*1.5,},
    '-205': {x2: (420 - 335)*1.5, x1: (420 - 410)*1.5,},
    '-238': {x2: (420 - 253)*1.5, x1: (420 - 341)*1.5,},
    '-261': {x2: (420 - 300)*1.5, x1: (420 - 388)*1.5,},
    '-301': {x2: (420 - 293)*1.5, x1: (420 - 386)*1.5,},
    '-328': {x2: (420 - 249)*1.5, x1: (420 - 323)*1.5,},
    '-353': {x2: (420 - 301)*1.5, x1: (420 - 380)*1.5,}

    // '-23': {x1: 248, x2: 336,}, // origin coord. on the 880map
    // '-57': {x1: 325, x2: 407,},
    // '-83': {x1: 302, x2: 383,},
    // '-118':{x1: 320, x2: 393,},
    // '-146': {x1: 289, x2: 356,},
    // '-173': {x1: 294, x2: 374,},
    // '-205': {x1: 335, x2: 410,},
    // '-238': {x1: 253, x2: 341,},
    // '-261': {x1: 300, x2: 388,},
    // '-301': {x1: 293, x2: 286,},
    // '-328': {x1: 249, x2: 323,},
    // '-353': {x1: 301, x2: 380,}
};
