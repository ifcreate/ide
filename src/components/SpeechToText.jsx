import React from 'react';
import { Button } from 'element-react';
import 'style/speech-to-text.less'
import TestWrapper from 'components/TestWrapper';

const TIMER = 10;

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: TIMER,
            showCount: false,
        }
        window.sttHideCount = this.hideCount.bind(this);
    }
    componentDidMount() {
        window.sttStartCount = () => {
            this.showCount();
            this.countDown(TIMER, true);
        }
    }
    countDown(n, showCount) {
        if (showCount && (n >= 0)) {
            this.setState({
                count: n,
            });
            if (n == 0) {
                this.submit();
            }
            setTimeout(() => {
                this.countDown(n - 1, this.state.showCount);
            }, 1000)
        }
    }
    showCount() {
        this.setState({
            showCount: true,
        })
    }
    hideCount() {
        this.setState({
            showCount: false,
        })
    }
    submit() {
        this.hideCount();
        this.props.start();
    }
    render() {
        const { count, showCount } = this.state;
        const disableButton = count == 0 || !showCount;
        return (
            <div className="speech-block ui-block top-block">
                <div className="block-title">Speech to Text</div>
                <div className="video-wrap">
                    <p
                        style={ {
                            maxHeight: '40px',
                            lineHeight: '20px',
                            overflow: 'auto',

                        } }
                    >{ this.props.text }</p>
                    <div className="button-wrap"
                        style={ {
                            textAlign: 'center',
                        } }
                    >
                        <Button
                            onClick={ () => {
                                this.submit();
                            } }
                            type="danger" size="small"
                            disabled={ disableButton }
                        >{ disableButton ?
                            `Stop`
                            : `Stop(${count}s)`
                            }</Button>
                    </div>
                    <TestWrapper>
                        <Button onClick={ () => { window.recorder.play(document.querySelector('audio')) } } type="success" size="small">载入录音</Button>
                    </TestWrapper>
                    <TestWrapper>
                        <audio controls autoplay></audio>
                    </TestWrapper>
                </div>
            </div>
        )
    }
}
