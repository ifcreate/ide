import React from 'react';
import 'style/cam.less'

export default function OutputImage (props) {
    const src = props.srcFileName ? `../src/images/${props.srcFileName}` : ''
    return (
        <div className="cam-block ui-block top-block">
            <div className="block-title">Image {props.children}</div>
            <div className="video-wrap">
                {props.srcFileName ? <img className="output-image" src={src} />
                    : <img className="output-image"/>
                }
            </div>
        </div>
    );
}
