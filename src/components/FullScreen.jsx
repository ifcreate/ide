import React, {Component} from 'react';
import 'style/fullscreen.less';
import interact from 'interactjs'

export default class FullScreen extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const inputcam = document.querySelector('#inputcam');
        const fscam = document.querySelector('#fs-cam');
        if (inputcam && fscam) {
            fscam.srcObject = inputcam.srcObject;
        }
        // drag n resize
        interact('.draggable').draggable({
            inertia: true,
            modifiers: [
                interact.modifiers.restrict({
                    restriction: "parent",
                    endOnly: true,
                    elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
                }),
            ],
            // enable autoScroll
            autoScroll: true,
            onmove: dragMoveListener,
        }).resizable({
            edges: { left: true, right: true, bottom: true, top: true },
            modifiers: [
                interact.modifiers.restrictEdges({
                    outer: 'parent',
                    endOnly: true,
                }),
                interact.modifiers.restrictSize({
                    min: { width: 100, height: 100 },
                }),
            ],
            inertia: true
        }).on('resizemove', resizeMoveListener);

        function dragMoveListener (event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
            target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';
            // update the position attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }
        function resizeMoveListener (event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0);
            // update the element's style
            target.style.width  = event.rect.width + 'px';
            target.style.height = event.rect.height + 'px';
            // translate when resizing from top or left edges
            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px)';
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }
        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;
        window.resizeMoveListener = resizeMoveListener;
    }
    componentWillUnmount() {
        const d = interact('.draggable');
        d.off('dragmove', window.dragMoveListener);
        d.off('resizemove', window.resizeMoveListener);
    }
    render() {
        return (
            <div className="fullscreen-wrap">
                <div className="fs-close-button" onClick={() => {
                    this.props.close()
                }}>X
                </div>
                <div className="fs-content">
                    {this.props.cam && <div className="fs-cam-wrap draggable">
                        <video id="fs-cam" width="100%" height="100%" autoPlay/>
                    </div>}
                    {this.props.image && <div className="draggable fs-img-wrap">
                        <img className="img-face" src={`../src/images/${this.props.srcFileName || 'Face.gif'}`} />
                    </div>
                    }
                </div>
            </div>
        );
    }
}
