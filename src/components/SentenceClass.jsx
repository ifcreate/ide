import React, {useState} from 'react';
import {Input, Button, Popover} from 'element-react';
import 'style/sentence-class.less';

export default function SentenceClass (props) {
    const info = props.classInfo || {};
    const [caseInput, setCaseInput] = useState('');
    return (
        <div className="box-wrap sentence-class-wrap ui-block bottom-block">
            <div className="title">{info.name}</div>
            <div className="label">Description</div>
            <div className="progress-wrap">
                <Input placeholder="Text..." value={info.desc} onChange={(text)=>props.descOnChange(props.index, text)}/>
            </div>
            <div className="label">Case: {info.cases.length} <span className="tips">(5 at least)</span></div>
            <div className="progress-wrap">
                <Input placeholder="Text..."
                       value={caseInput}
                       onChange={(v)=>{setCaseInput(v);}}
                       onKeyDown={(e)=>{
                           if (caseInput == '') return;
                           if (e.keyCode == 13) { // enter
                               props.addCase(e, props.index, caseInput);
                               setCaseInput('');
                           }
                       }}
                />
            </div>
            <Popover placement="right" title="" width="200" trigger="click" content={(
                <div className="edit-wrap">
                    {info.cases.map((item, i) => {
                        return <div className="edit-line">
                            <span className="index">{i+1}</span> <span className="sentence">{item}</span> <span className="edit-remove" onClick={()=>{props.removeCase(props.index, i)}}>-</span>
                        </div>
                    })}
                </div>
            )}>
                <Button type="primary"  size="mini" style={{backgroundColor: info.color, borderColor: info.color, marginTop:6}}>Edit Sentence List</Button>
            </Popover>
        </div>
    );
}

