import React from 'react';
import {Message} from 'element-react';
import {checkVideo} from "lib/utils";
import 'style/reg-train.less';

const WIDTH = 110;
const HEIGHT = 80;

export default class RegressionTrain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            pos0: '0%',
            pos1: '0%',
            pos2: '0%',
            pos: '0%',
            loss: '--',
        };
        this.slider = React.createRef();
        this.canvasRef0 = React.createRef();
        this.canvasRef1 = React.createRef();
        this.canvasRef2 = React.createRef();
    }
    componentDidMount() {

    }

    componentWillUnmount() {
        this.props.stopReg();
    }
    addImage () {
        const {count} = this.state;
        if (!checkVideo(this.props.video)) {
            return;
        }
        const pos = count % 3;
        const val = this.getSliderPercent();
        let context;
        if (pos === 0) {
            this.setState({
                pos0: val,
            });
            context = this.canvasRef0.current.getContext('2d')
        } else if (pos === 1) {
            this.setState({
                pos1: val,
            });
            context = this.canvasRef1.current.getContext('2d')
        } else if (pos === 2) {
            this.setState({
                pos2: val,
            });
            context = this.canvasRef2.current.getContext('2d')
        }
        context.save();
        context.scale(-1,1); //mirror
        context.drawImage(this.props.video, 0, 0, -WIDTH, HEIGHT);
        context.restore();
        this.props.knn.addImage(this.slider.current.value);
        this.setState({
            count: count + 1,
        });
    };
    getSliderPercent() {
        return Math.round(this.slider.current.value * 100) + '%';
    }
    train() {
        try {
            Message.warning('训练中，请等待...');
            this.props.knn.train((lossValue) => {
                if (lossValue) {
                    this.setState({
                        loss: lossValue,
                    });
                } else {
                    Message.success('训练完成！');
                }
            });
        } catch (e) {
            Message.error(e.message);
        }
    }
    updatePos() {
        this.setState({
            pos:this.getSliderPercent(),
        });
    }
    render() {
        const {count, pos, pos0, pos1, pos2, loss} = this.state;
        return (
            <div className="class-train-wrap reg-train-wrap ui-block bottom-block">
                <div className="class-title-line">
                    <div className="class-title">{pos}</div>
                    {/*<button onClick={reset}>Reset</button>*/}
                </div>
                <div className="slider-wrap">
                    <input type="range" min="0.01" max="1" defaultValue="0" step="0.01" className="slider" id="slider"
                           ref={this.slider} onChange={()=>this.updatePos()}/>
                </div>
                <div className="reg-canvas-tot flex-wrap">
                    <div className="canvas-reg-wrap">
                        <canvas id={"canvas0"} className="canvas-regression" ref={this.canvasRef0} width={WIDTH}
                                height={HEIGHT}/>
                        <div className="reg-img-label">{pos0}</div>
                    </div>
                    <div className="canvas-reg-wrap">
                        <canvas id={"canvas1"} className="canvas-regression" ref={this.canvasRef1} width={WIDTH}
                                height={HEIGHT}/>
                        <div className="reg-img-label">{pos1}</div>
                    </div>
                    <div className="canvas-reg-wrap">
                        <canvas id={"canvas2"} className="canvas-regression" ref={this.canvasRef2} width={WIDTH}
                                height={HEIGHT}/>
                        <div className="reg-img-label">{pos2}</div>
                    </div>
                </div>
                <div className="class-sample-count">Samples: {count}, Final loss: {loss}</div>
                <div className="reg-train-buttons flex-wrap">
                    <span className="class-button-train" onClick={()=>this.addImage()}>Add sample</span>
                    <span className="class-button-train" onClick={()=>this.train()}>Train</span>
                    {!this.props.predicting && <span className="class-button-train" onClick={()=>this.props.predict()}>Predict</span>}
                    {this.props.predicting && <span className="class-button-train" onClick={()=>this.props.stopReg()}>Stop</span>}
                </div>
            </div>
        );
    }
}
