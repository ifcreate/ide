import React from 'react';
import {
    G2,
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
    Label,
    Legend,
    View,
    Guide,
    Shape,
    Util
} from "bizcharts";
import 'style/scores.less';

export default function Scores (props) {
    const data = props.scores.map((item, index) => {
        return {
            value: item,
            index
        }
    })
    const cols = {
        value: {
            min: 0,
            max: 5, // 定义数值范围的最大值
            tickInterval: 2, // 用于指定坐标轴各个标度点的间距，是原始数据之间的间距差值，tickCount 和 tickInterval 不可以同时声明。
        },
        index: {
            range: [0, 1]
        }
    };
    return (
        <div className="box-wrap score-wrap">
            <div className="title">最近得分</div>
            <div className="chart">
                <Chart height={150} data={data} scale={cols} forceFit padding={'auto'}>
                    <Axis name="index" visible={false}/>
                    <Axis name="value" />
                    <Geom type="line" position="index*value" size={2} />
                    <Geom
                        type="point"
                        position="index*value"
                        size={4}
                        shape={"circle"}
                        style={{
                            stroke: "#fff",
                            lineWidth: 1
                        }}
                    />
                </Chart>
            </div>
        </div>
    );
}
