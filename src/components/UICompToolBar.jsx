import React, {Component} from 'react';
import {Dropdown} from 'element-react';
import 'style/ui-comp-toolbar.less';

export default class UICompToolBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tools: [
                {label: 'Image', innerName: 'output_image'}, // innerName is for state control
                {label: 'Game', innerName: 'game_block'},
            ],
            sendToRobot: false,
        }
    }
    render() {
        return (
            <div className="ui-comp-toolbar-wrap">
                {this.state.tools.map((item) => {
                    if (item.label == 'Game') {
                        return (
                            <Dropdown
                                key={item.label}
                                // splitButton // bugging
                                size="large"
                                onCommand={(cmd)=>{this.props.toolOnClick(cmd)}} menu={(
                                <Dropdown.Menu>
                                    <Dropdown.Item divided command="dinosaur_game" key="a">Dinosaur Game</Dropdown.Item>
                                    <Dropdown.Item divided command="robi_walking" key="b">Robi Walking</Dropdown.Item>
                                    <Dropdown.Item divided command="robi_race" key="c">Robi Race</Dropdown.Item>
                                    <Dropdown.Item divided command="robi_reception" key="d">Robi Reception</Dropdown.Item>
                                </Dropdown.Menu>
                            )}>
                                <div className="tool-wrap" onClick={() => {}} key={item.label}>
                                    {item.label}<i className="el-icon-caret-bottom el-icon--right"></i>
                                </div>
                            </Dropdown>
                        )
                    } else {
                        return <div className=" tool-wrap" onClick={() => this.props.toolOnClick(item.innerName)} key={item.label}>
                            {item.label}
                        </div>
                    }
                })}
            </div>
        );
    }
}
