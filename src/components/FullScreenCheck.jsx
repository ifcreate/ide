import React from 'react';
import 'style/fullscreencheck.less';

export default function FullScreenCheck (props) {
    return (
        <div className={`fs-check-wrap${props.checked ? ' checked' : ''}`} onClick={()=>{props.clickHandler()}} />
    );
}
