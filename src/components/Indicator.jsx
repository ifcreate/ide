import React, {Component} from 'react';
import 'style/output.less';

export default class Indicator extends Component {
    componentDidMount() {
    }
    render() {
        const {name='', color='#E0E0E0', desc=''} = this.props.classInfo || {}; // desc has higher prio
        return (
            <div className="output-wrap box-narrow class-train-wrap ui-block bottom-block indicator-wrap">
                <div className="class-title-line">
                    <div className="class-title">{this.props.title}</div>
                </div>
                <div id={this.props.name+'_color'} className="result-wrap" style={{backgroundColor:color}}>
                    <div id={this.props.name+'_text'} className="result-name">{desc || name}</div>
                </div>
            </div>
        );
    }
}
