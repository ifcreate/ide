import React, {useState} from 'react';
import {Progress} from 'element-react';
import 'style/curling.less';
import {trainingProgress} from "lib/CurlingRobot";

const TARGET_NUM = 5;

export default function Curling (props) {
    // const [active, setActive] = useState(0);
    return (
        <div className="curling-block ui-block top-block">
            <div className="block-title">目标位置1</div>
            <div className="content-wrap">
                <div className="image-wrap">
                    <img className="img-robi" src={`../src/images/${props.srcFileName || 'robi.png'}`} />
                    <div className="text-progress">训练进度：</div>
                </div>
                {
                    new Array(TARGET_NUM).fill(0).map((item, index) => {
                        return (
                            <div className="target-wrap">
                                <div className={`target-button${props.target == (index+1) ? ' active' : ''}`}
                                     onClick={()=>props.targetOnClick(index + 1)}
                                >{index + 1}</div>
                                <Progress type="circle"
                                          percentage={trainingProgress(index)}
                                          width={40} strokeWidth={2}
                                          className="target-progress"
                                />
                            </div>
                        )
                    })
                }
                {
                    <div className="curling-stone">
                        <img src={`../src/images/${'curling_stone.png'}`} />
                    </div>
                }
                {props.children}
            </div>
        </div>
    );
}
