import React from 'react';
import {parseQuery} from '../lib/utils';

export default function TestWrapper (props) {
    if (parseQuery(location.search).test) {
        return (
            props.children
        );
    }
    return null;
}
