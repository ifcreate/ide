const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const htmlPluginList = require('./build/genhtml')();
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const entries = require('./build/entry');
const webpack = require('webpack');
const devMode = process.env.NODE_ENV !== 'production';

for (const entry in entries) {
    if (entry) {
        entries[entry] = ['react-hot-loader/patch', entries[entry]]
    }
}

module.exports = {
    entry: entries,
    output: {
        filename: '[name].[hash:8].js',
        // filename: '[name].js',
        // publicPath: "/static",
        path: path.resolve(__dirname, 'dist')
    },
    // devtool: 'source-map',
    plugins: [ //todo common chunk
        new CleanWebpackPlugin(),
        ...htmlPluginList,
        new MiniCssExtractPlugin({
            filename: devMode ? '[name].css' : '[name].[hash].css',
            chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        }),
        // new webpack.HotModuleReplacementPlugin(),
        // new webpack.NamedModulesPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.(jsx|js)?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        cacheDirectory: true,
                        plugins: ['react-hot-loader/babel'],
                    }
                },
                include: [
                    path.resolve(__dirname, 'src'),
                ],
            }, {
                test: /\.less/,
                use: [
                    devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { importLoaders: 1, modules: false } },
                    'postcss-loader',
                    'less-loader',
                ],
            }, {
                test: /\.css/,
                use: ['style-loader', 'css-loader'],
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 1024,
                        name: '[name].[ext]',
                        publicPath: '../src/images'
                    },
                }],
            }, {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                }
            },
        ]
    },
    devServer: {
        // contentBase: './',
        port: 3000,
        // publicPath: "http://localhost:3000/dist/",
        // hot: true
    },
    resolve: {
        extensions: ['.jsx', '.js'],
        alias: {
            'style': path.join(__dirname, 'src/style'),
            'pages': path.join(__dirname, 'src/pages'),
            'components': path.join(__dirname, 'src/components'),
            'lib': path.join(__dirname, 'src/lib')
        },
    },
};
