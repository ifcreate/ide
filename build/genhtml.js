const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const entries = require('./entry');

module.exports = () => {
    const chunkNames = Object.keys(entries);
    const htmlList = [];
    for (const chunkName of chunkNames) {
        // 忽略common chunk
        if (chunkName === 'common') {
            continue;
        }
        htmlList.push(new HtmlWebpackPlugin({
            title: ' ',
            // filename: '../html/' + chunkName + '.html', // relative to output path
            filename: chunkName + '.html',
            chunks: [chunkName],
            // hash: true,
            inject: 'body',
            template: require('html-webpack-template'),
            appMountId: 'app',
            // chunkName: chunkName, //
            template: path.join(__dirname, '../src/index.ejs'),
            ide: chunkName == 'ide',
            // chunks: ['manifest', 'common', chunkName]
        }));
    }

    return htmlList;
};


